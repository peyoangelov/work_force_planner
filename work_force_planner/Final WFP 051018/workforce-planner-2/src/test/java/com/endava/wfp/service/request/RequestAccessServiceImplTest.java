package com.endava.wfp.service.request;

import com.endava.wfp.dto.RequestDTO;
import com.endava.wfp.utility.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestAccessServiceImplTest {

    @Autowired
    private RequestAccessServiceImpl requestAccessService;

    /**
     * Checks if you can find
     * a request by
     * approval status
     */
    @Test
    public void findByStatusTest() {
        List<RequestDTO> list = this.requestAccessService.findByStatus(Constants.STATUS_PENDING);
        assertNotNull(list);
    }

    /**
     * Negative test for
     * a request for
     * approval status
     */
    @Test(expected = UsernameNotFoundException.class)
    public void requestStatusExceptionTest() {
        this.requestAccessService.requestStatus(Constants.STATUS_APPROVED, -1L);
    }
}