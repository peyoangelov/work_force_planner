package com.endava.wfp.enums;

import org.junit.Assert;
import org.junit.Test;

public class DisciplinesTest {

    @Test
    public void fromStringJavaTest() {

        String javaValue = "Java";
        String javaName = "JAVA";

        Assert.assertEquals(javaValue, Discipline.fromString(javaName).getValue());
        Assert.assertEquals(javaName, Discipline.JAVA.name());
    }

    @Test
    public void fromStringDotNetTest() {

        String dotNetValue = "dotNET";
        String dotNetName = "dotNET";

        Assert.assertEquals(dotNetValue, Discipline.fromString(dotNetName).getValue());
        Assert.assertEquals(dotNetName, Discipline.dotNET.name());
    }

    @Test
    public void fromStringSeniorTest() {

        String qaValue = "QA";
        String qaName = "QA";

        Assert.assertEquals(qaValue, Discipline.fromString(qaName).getValue());
        Assert.assertEquals(qaName, Discipline.QA.name());
    }
}
