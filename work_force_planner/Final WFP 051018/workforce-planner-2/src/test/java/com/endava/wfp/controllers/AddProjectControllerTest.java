package com.endava.wfp.controllers;

import com.endava.wfp.service.project.ProjectService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureMockMvc
@ContextConfiguration
public class AddProjectControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Mock
    private ProjectService projectService;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    /**
     * test
     * given add project view
     * when mock mvc
     * then verify response
     * with user (not manager)
     *
     * @throws Exception
     */
    @Test
    @WithMockUser
    public void givenAddProjectUri_whenMockMvc_thenReturnsAddProjectView() throws Exception {

        this.mockMvc.perform(get("/project/add"))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    /**
     *
     *
     * @throws Exception
     */
    @Test
    public void givenAddProjectUri_whenNoUser_thenRedirectToLoginPage() throws Exception {

        this.mockMvc.perform(get("/project/add"))
                .andDo(print())
                .andExpect(status().isFound())
                .andReturn();
    }
}