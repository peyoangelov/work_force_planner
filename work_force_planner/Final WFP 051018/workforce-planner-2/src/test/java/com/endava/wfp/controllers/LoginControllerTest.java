package com.endava.wfp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureWebTestClient
@WebAppConfiguration
@ContextConfiguration
@AutoConfigureMockMvc
public class LoginControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }


    /**
     * Checks if the
     * login view loads
     * @throws Exception
     */
    @Test
    public void test_access_login_url() throws Exception {
        mvc
                .perform(get("/login")).andExpect(status().isOk());
    }

    /**
     *
     * @throws Exception
     * Checks if the user
     * exists in the database
     */
    @Test
    public void testing_with_user_valid() throws Exception {
        mvc
                .perform(post("/login")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("email","amcdell0@etsy.com")
                        .param("password", "password"))
                .andExpect(status().is(302))
                .andExpect(header().exists("Location"))
                .andExpect(header().string("Location", new BaseMatcher<String>() {
                    @Override
                    public boolean matches(Object o) {
                        if (o instanceof String) {
                            String foundLocation = (String)o;
                            return foundLocation.endsWith("/dashboard");
                        }
                        return false;
                    }

                    @Override
                    public void describeTo(Description description) {
                        log.info("Method is not used but require to be override");
                    }
                }));
    }

    /**
     *
     * @throws Exception
     * The Negative test
     * for user login
     */
    @Test
    public void testing_with_user_invalid() throws Exception {
        mvc
                .perform(post("/login")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("email","amcdell0@etsy.com")
                        .param("password", "password1"))
                .andExpect(status().is(302))
                .andExpect(header().exists("Location"))
                .andExpect(header().string("Location", new BaseMatcher<String>() {
                    @Override
                    public boolean matches(Object o) {
                        if (o instanceof String) {
                            String foundLocation = (String)o;
                            return foundLocation.endsWith("/login?error=true");
                        }
                        return false;
                    }

                    @Override
                    public void describeTo(Description description) {
                        log.info("Method is not used but require to be override");
                    }
                }));
    }
}