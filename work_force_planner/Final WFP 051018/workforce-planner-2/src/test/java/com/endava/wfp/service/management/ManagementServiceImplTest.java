package com.endava.wfp.service.management;

import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.dto.ManagementDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.EmployeeHistory;
import com.endava.wfp.entity.EmployeePosition;
import com.endava.wfp.enums.Action;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.utility.Constants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ManagementServiceImplTest {

    @Autowired
    private ManagementService managementService;
    @Autowired
    private EmployeeRepository employeeRepository;

    private Pageable pageable;
    private Employee employee;
    private EmployeeEditDTO dto;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        pageable = PageRequest.of(1, Constants.EMPLOYEE_PER_PAGE);
        employee = new Employee();
        Employee manager = this.employeeRepository.findById(1L)
                .orElseThrow(() -> new UsernameNotFoundException("User is not found"));
        employee.setManager(manager);
        employee.setEmployeesPositions(new EmployeePosition(null, Grade.JUNIOR, Discipline.JAVA, Status.CURRENT_BENCH, employee, new ArrayList<>()));
        employee.setPassword("pass");
        employee.setEmail("email");
        employee.setFirstName("firstName");
        employee.setLastName("lastName");
        this.employeeRepository.save(employee);
        dto = new EmployeeEditDTO();
        dto.setDiscipline(Discipline.JAVA.getValue());
        dto.setEmail("email");
        dto.setGrade(Grade.JUNIOR.getValue());
        dto.setStatus(Status.BENCH_IN_ONE_MONTH.getValue());
        dto.setPassword("pass");
    }

    /**
     * Checks if you can
     * get the whole
     * history
     */
    @Test
    public void findAllHistoryRecordsTest() {
        Page<ManagementDTO> managementList = this.managementService.findAll(pageable);
        assertEquals(managementList.getTotalElements(), 0);
        assertEquals(managementList.getTotalPages(), 0);
        assertNotNull(this.managementService.findAll(pageable));
    }

    /**
     * Checks if you can
     * audit an ADD action
     * in the history table
     */
    @Test
    public void saveHistoryAuditTest() {
        EmployeeHistory employeeHistory = this.managementService.saveHistoryAudit(employee);
        assertEquals(employeeHistory.getAction(), Action.INSERTED);
        assertEquals(employeeHistory.getChangeRecord().size(), 4);
    }

    /**
     * Checks if you can
     * audit an EDIT action
     * in the history table
     */
    @Test
    public void editEmployeeAuditTest() {
        Employee employee = this.employeeRepository.findById(6000L)
                .orElseThrow(() -> new UsernameNotFoundException("Employee has not been found"));
        employee.setEmployeesProjects(new ArrayList<>());
        EmployeeHistory employeeHistory = this.managementService.editEmployeeAudit(employee, dto);
        assertEquals(employeeHistory.getAction(), Action.UPDATED);
        assertEquals(employeeHistory.getChangeRecord().size(), 5);
    }

    /**
     * Checks if you can
     * audit an DELETE action
     * in the history table
     */
    @Test
    public void deleteEmployeeAuditTest() {
        Employee employee = this.employeeRepository.findById(6000L)
                .orElseThrow(() -> new UsernameNotFoundException("Employee has not been found"));
        EmployeeHistory employeeHistory = this.managementService.deleteEmployeeAudit(employee);
        assertEquals(employeeHistory.getAction(), Action.DELETED);
        assertEquals(employeeHistory.getChangeRecord().size(), 4);
    }
}