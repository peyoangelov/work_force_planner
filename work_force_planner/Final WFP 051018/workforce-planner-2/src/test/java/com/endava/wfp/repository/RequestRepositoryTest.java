package com.endava.wfp.repository;


import com.endava.wfp.entity.Request;
import com.endava.wfp.enums.RequestStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.endava.wfp.utility.Constants.TEST_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest

public class RequestRepositoryTest {

    @Autowired
    private RequestRepository requestRepository;

    /**
     * Test to save request to the db
     * try to retrieve the request
     * check if retrieved request is not empty
     * check if all the fields match
     */
    @Test
    public void test_saveAndRetrieveRequests () {

        Request request = requestRepository.save(createRequest(TEST_ID, "test@test.com"));
        Optional<Request> retrievedRequest = requestRepository.findById(TEST_ID);

        assertNotNull(retrievedRequest);
        assertEquals(request.getId(), retrievedRequest.get().getId());
        assertEquals(request.getFirstName(), retrievedRequest.get().getFirstName());
        assertEquals(request.getLastName(), retrievedRequest.get().getLastName());
        assertEquals(request.getEmail(), retrievedRequest.get().getEmail());
        assertEquals(request.getReasonForRequest(), retrievedRequest.get().getReasonForRequest());
        assertEquals(request.getStatus(), retrievedRequest.get().getStatus());
        assertEquals(request.getRequestDate(), retrievedRequest.get().getRequestDate());
    }

    /**
     * Test to save 2 request to the db
     * there is email constraint so
     * the emails should be unique
     * check the size of the retrieved list with requests
     */
    @Test
    public void test_countRequests() {

        requestRepository.save(createRequest(TEST_ID, "test1@test.com"));
        requestRepository.save(createRequest(2l, "test2@test.com"));

        List<Request> retrievedRequests = requestRepository.findAll();
        assertEquals(2, retrievedRequests.size());

    }

    /**
     * create and save request to the db
     * retrieve the request from the db
     * delete it and try to find it again
     */
    @Test
    public void test_deleteRequest(){

        requestRepository.save(createRequest(TEST_ID,"test@test.com"));
        Optional<Request> retriеvedRequest = requestRepository.findById(TEST_ID);

        assertEquals(true,retriеvedRequest.isPresent());

        requestRepository.deleteById(TEST_ID);
        retriеvedRequest = requestRepository.findById(TEST_ID);

        assertEquals(false,retriеvedRequest.isPresent());

    }


    /**
     * create request
     * @param l random request id
     * @param email employee email
     * @return new request
     */
    private Request createRequest (long l, String email) {
        return new Request(l, "first name", "last name", email,
                "reason for request", RequestStatus.PENDING, LocalDateTime.now(), null, null, null);
    }

}
