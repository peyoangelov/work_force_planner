package com.endava.wfp.enums;

import org.junit.Assert;
import org.junit.Test;

public class StatusesTest {

    @Test
    public void fromStringBookedTest() {

        String bookedValue = "Booked";
        String bookedName = "BOOKED";

        Assert.assertEquals(bookedValue, Status.fromValue(bookedValue).getValue());
        Assert.assertEquals(bookedName, Status.BOOKED.name());
    }

    @Test
    public void fromStringCurrentBenchTest() {

        String currentBenchValue = "Current bench";
        String currentBenchName = "CURRENT_BENCH";

        Assert.assertEquals(currentBenchValue, Status.fromValue(currentBenchValue).getValue());
        Assert.assertEquals(currentBenchName, Status.CURRENT_BENCH.name());
    }

    @Test
    public void fromStringBenchInOneMonthTest() {

        String benchInOneMonthValue = "Bench in one month";
        String benchInOneMonthName = "BENCH_IN_ONE_MONTH";

        Assert.assertEquals(benchInOneMonthValue, Status.fromValue(benchInOneMonthValue).getValue());
        Assert.assertEquals(benchInOneMonthName, Status.BENCH_IN_ONE_MONTH.name());
    }

    @Test
    public void fromStringBenchInTwoMonthsTest() {

        String benchInTwoMonthsValue = "Bench in two months";
        String benchInTwoMonthsName = "BENCH_IN_TWO_MONTHS";

        Assert.assertEquals(benchInTwoMonthsValue, Status.fromValue(benchInTwoMonthsValue).getValue());
        Assert.assertEquals(benchInTwoMonthsName, Status.BENCH_IN_TWO_MONTHS.name());
    }

    @Test
    public void fromStringBookedInOneMonthTest() {

        String bookedInOneMonthValue = "Booked in one month";
        String bookedInOneMonthName = "BOOKED_IN_ONE_MONTH";

        Assert.assertEquals(bookedInOneMonthValue, Status.fromValue(bookedInOneMonthValue).getValue());
        Assert.assertEquals(bookedInOneMonthName, Status.BOOKED_IN_ONE_MONTH.name());
    }
}
