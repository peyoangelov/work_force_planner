package com.endava.wfp.controllers;


import com.endava.wfp.dto.EmployeePageDTO;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.service.user.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureMockMvc
@ContextConfiguration
public class PaginationControllerTest {

    @Autowired
    private EmployeePagination employeePaginationTest;

    @Mock
    private EmployeeService employeeServiceTest;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser
    public void listEmployees_hasAllModelAttributes() throws Exception {

        this.mvc.perform(get("/list/{page}/{sortBy}", 1, "firstName"))
                .andExpect(status().isOk())
                .andExpect(model()
                        .attributeExists("employeeList", "pageCreator", "pageNumber", "sortByCode"))
                .andReturn();
    }

    @Test
    @WithMockUser
    public void getListOfEmployees_test() throws Exception {
        this.mvc.perform(get("/list/{page}/{sortBy}", 1, "firstName"))
                .andExpect(status().isOk())
                .andExpect(view().name("list"))
                .andReturn();
    }

    @Test
    @WithMockUser
    public void getListOfEmployees_redirectsToPreviousPageIfCurrentIsEmpty() throws Exception {

        Pageable pageable = PageRequest.of(5, 10);
        Page<EmployeePageDTO> employeeList = employeePaginationTest.findAll(pageable);

        if (!employeeList.hasContent()) {
            this.mvc.perform(get("/list/{page}/{sortBy}", employeeList.getTotalPages(), "firstName"))
                    .andExpect(view().name("redirect:/list/" + (employeeList.getNumber() - 1) + "/id"))
                    .andReturn();
        }
    }
}


