package com.endava.wfp.service.user;

import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.utility.exception.UsedEmailException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static com.endava.wfp.utility.Constants.TEST_ID;
import static org.junit.Assert.*;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceImplH2Test {

    @Autowired
    private EmployeeServiceImpl employeeService;

    private EmployeeAddDTO dto;

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        dto = new EmployeeAddDTO();
        dto.setEmail("email@abv.bg");
        dto.setDiscipline("Java");
        dto.setFirstName("FirstName");
        dto.setLastName("LastName");
        dto.setGrade("SENIOR");
        dto.setManagerId(1L);
        dto.setPassword("pass123");
        dto.setProject("Flowdesk");
        dto.setSkill("Spring");
        dto.setStatus("Booked");
        dto.setStartDate(LocalDate.of(2017,9,14));
        dto.setStartDate(LocalDate.of(2017,9,20));
    }

    /**
     * Checks if the
     * an user is
     * a manager
     */
    @Test
    public void isManagerTest() {
        assertTrue(this.employeeService.isManager(1L));
        assertFalse(this.employeeService.isManager(6000L));
    }

    /**
     * Checks if
     * you can find an
     * user by his id
     */
    @Test
    public void findByIdTest() {
        Employee employee = this.employeeService.findById(1L);
        assertEquals(employee.getId(), Long.valueOf(1));
    }

    /**
     * Negative case
     * for findByID
     */
    @Test(expected = UsernameNotFoundException.class)
    public void findByIdTrowExceptionTest() {
        this.employeeService.findById(-1L);
    }

    /**
     * Checks if you can find
     * all managers and put
     * them in a list
     */
    @Test
    public void getAllManagersTest() {
        List<Employee> dtoList = this.employeeService.getAllManagers();
        assertNotNull(dtoList);
    }

    /**
     * Checks if you can
     * delete an user
     */
    @Test(expected = UsernameNotFoundException.class)
    public void deleteTest() {
        this.employeeService.delete(5000L);
        this.employeeService.findById(5000L);
    }

    /**
     * Checks if you
     * can load an user
     * by his username
     */
    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsernameTest() {
        this.employeeService.loadUserByUsername("email");
    }

    /**
     * Tests if you
     * can add a user
     * and then if he is
     * added in the database
     */
    @Test
    public void addTest() {
        Employee employee = this.employeeService.add(dto);
        assertEquals(employee.getEmail(), dto.getEmail());
        assertEquals(employee.getFirstName(), dto.getFirstName());
        assertEquals(employee.getLastName(), dto.getLastName());
        assertEquals(employee.getManager().getId(), dto.getManagerId());
        assertEquals(employee.getEmployeesPositions().getDiscipline().getValue(), dto.getDiscipline());
        assertEquals(employee.getEmployeesPositions().getGrade().getValue(), Grade.SENIOR.getValue());
        assertEquals(employee.getEmployeesPositions().getStatus().getValue(), dto.getStatus());
        assertEquals(employee.getEmployeesPositions().getSkills().get(0).getName(), dto.getSkill());
        assertEquals(employee.getEmployeesProjects().get(0).getProject().getName(), dto.getProject());
    }

    /**
     * Checks if you can
     * save an employee
     *
     */
    @Test(expected = UsedEmailException.class)
    public void givenDtoWithEmailInUseUsedEmailExceptionIsThrown() {
        Employee emp = new Employee();
        emp.setFirstName("Ivan");
        emp.setLastName("Petrov");
        emp.setEmail("ivanpetrov@gmail.com");
        emp.setPassword("password");
        emp.setManager(employeeRepository.findById(TEST_ID).get());
        employeeRepository.save(emp);

        employeeService.checkForExistingEmail(emp.getEmail());
    }
}
