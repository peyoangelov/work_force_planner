package com.endava.wfp.controllers;

import com.endava.wfp.service.position.EmployeePositionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AllocationStatusControllerIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private AllocationStatusController controllerToTest;

    @MockBean
    private EmployeePositionService service;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerToTest).build();
    }


    /**
     *
     * @throws Exception
     * Checks if the specified
     * url exists
     */
    @Test
    public void givenURI_whenMockMVC_thenReturnsViewName() throws Exception {
        this.mockMvc.perform(get("/allocationstatus")).andDo(print())
                .andExpect(view().name("charts/allocationstatus"));
    }

    /**
     * @throws Exception
     * Checks if the status
     * is taken from the url
     * and if the charset
     * is UTF-8 and the
     * url chart content
     * is json
     */
    @Test
    public void givenStatusCurrentBench_whenMockMVC_thenReturnsArray() throws Exception {
       this.mockMvc.perform(get("/getAllocationDataForChart?status=Current bench"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").isArray()).andReturn();
    }

    /**
     * @throws Exception
     * Checks if the status
     * is taken from the url
     * and if the charset
     * is UTF-8 and the
     * url chart content
     * is json
     */
    @Test
    public void givenStatusBenchInOneMonth_whenMockMVC_thenReturnsArray() throws Exception {
         this.mockMvc.perform(get("/getAllocationDataForChart?status=Bench in one month"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").isArray()).andReturn();
    }
    /**
     * @throws Exception
     * Checks if the status
     * is taken from the url
     * and if the charset
     * is UTF-8 and the
     * url chart content
     * is json
     */
    @Test
    public void givenStatusBenchInTwoMonth_whenMockMVC_thenReturnsArray() throws Exception {
         this.mockMvc.perform(get("/getAllocationDataForChart?status=Bench in two months"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").isArray()).andReturn();
    }
    /**
     * @throws Exception
     * Checks if the status
     * is taken from the url
     * and if the charset
     * is UTF-8 and the
     * url chart content
     * is json
     */
    @Test
    public void givenStatusBooked_whenMockMVC_thenReturnsArray() throws Exception {
        this.mockMvc.perform(get("/getAllocationDataForChart?status=Booked"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").isArray()).andReturn();
    }
    /**
     * @throws Exception
     * Checks if the status
     * is taken from the url
     * and if the charset
     * is UTF-8 and the
     * url chart content
     * is json
     */
    @Test
    public void givenStatusBookedInOneMonth_whenMockMVC_thenReturnsArray() throws Exception {
        this.mockMvc.perform(get("/getAllocationDataForChart?status=Booked in one month"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").isArray()).andReturn();
    }
}

