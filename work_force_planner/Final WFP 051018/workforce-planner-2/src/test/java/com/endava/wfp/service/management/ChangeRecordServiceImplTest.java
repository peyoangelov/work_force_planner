package com.endava.wfp.service.management;

import com.endava.wfp.entity.ChangeRecord;
import com.endava.wfp.entity.EmployeeHistory;
import com.endava.wfp.enums.Action;
import com.endava.wfp.repository.ManagementRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ChangeRecordServiceImplTest {

    @Autowired
    private ChangeRecordServiceImpl changeRecordService;

    @Autowired
    private ManagementRepository managementRepository;

    private List<ChangeRecord> changeRecords;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        changeRecords = new ArrayList<>();
        EmployeeHistory empHistory = new EmployeeHistory();
        empHistory.setAction(Action.UPDATED);
        empHistory.setEmployeeId(6000L);
        EmployeeHistory employeeHistory = this.managementRepository.save(empHistory);
        changeRecords.add(new ChangeRecord("firstName", "First Name", null, employeeHistory));
    }

    /**
     * Check if you can save
     * an action and if it
     * appears in the database
     */
    @Test
    public void saveAllTest() {
        List<ChangeRecord> list =  this.changeRecordService.saveAll(changeRecords);
        assertNotNull(list);
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getPropertyName(), "firstName");
        assertEquals(list.get(0).getEmployeeHistory().getAction(), Action.UPDATED);
        assertEquals(list.get(0).getOldValue(), "First Name");
        assertNull(list.get(0).getNewValue());
    }
}