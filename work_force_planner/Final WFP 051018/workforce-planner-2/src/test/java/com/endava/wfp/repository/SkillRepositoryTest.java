package com.endava.wfp.repository;

import com.endava.wfp.entity.Skill;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static com.endava.wfp.utility.Constants.TEST_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class SkillRepositoryTest {

    @Autowired
    private SkillRepository skillRepository;

    /**
     * create and save skill to the db
     * retrieve the skill from the db
     * check if the names matches
     */
    @Test
    public void findByNameTest() {

        Skill skill = createSkill(null, "name", "description");
        assertEquals(skill, skillRepository.findByName("name"));

    }

    /**
     * create skill and save it to the db
     * try to retrieve the skill
     * check if retrieved skill is not empty
     * check if all the fields match
     */
    public void saveAndRetrieveSkillTest() {

        Skill skill = createSkill(TEST_ID, "name", "description");

        Optional<Skill> foundEntity = skillRepository.findById(TEST_ID);

        assertNotNull(foundEntity);
        assertEquals(skill.getId(), foundEntity.get().getId());
        assertEquals(skill.getName(), foundEntity.get().getName());
        assertEquals(skill.getDescription(), foundEntity.get().getDescription());

    }


    @Test
    public void countEntriesSkillRepositoryTest() {

        skillRepository.save(createSkill(11L,"name","description"));
        List<Skill> retrievedSkills = skillRepository.findAll();
        assertEquals(11, retrievedSkills.size());
    }

    /**
     * create and save skill to the db
     * retrieve the skill from the db
     * delete it and try to find it again
     */
    @Test
    public void deleteSkillEntityTest() {

        Skill skill = createSkill(null, "name", "description");
        Optional<Skill> savedSkill = skillRepository.findById(skill.getId());

        assertEquals(true, savedSkill.isPresent());

        skillRepository.deleteById(savedSkill.get().getId());
        savedSkill = skillRepository.findById(skill.getId());

        assertEquals(false, savedSkill.isPresent());

    }

    /**
     * create skill
     * @param testId skill id
     * @param name name of the skill
     * @param description description of the skill
     * @return new skill
     */
    private Skill createSkill (Long testId, String name, String description) {
        return skillRepository
                .save(new Skill(testId, name, description));
    }


}
