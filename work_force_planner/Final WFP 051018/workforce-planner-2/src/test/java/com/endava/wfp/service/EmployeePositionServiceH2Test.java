package com.endava.wfp.service;


import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Status;
import com.endava.wfp.service.position.EmployeePositionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit tests for all methods
 * in EmployeePositionService
 * which are used in
 * Allocation Status' page
 * with H2 in memory
 * database
 */

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeePositionServiceH2Test {

    @Autowired
    EmployeePositionService employeePositionService;

    /**
     *
     * Test
     * the count
     * by current bench
     * status
     */
    @Test
    public void countByStatusTest() {

        Assert.assertEquals(1304, employeePositionService.numberByStatus(Status.CURRENT_BENCH).longValue());

    }

    /**
     *
     * Test
     * the count
     * by booked
     * status
     */
    @Test
    public void countByStatusAndDisciplineTest() {

        Assert.assertEquals(428, employeePositionService.numberByStatusAndDiscipline(Status.BOOKED, Discipline.QA).longValue());

    }


}