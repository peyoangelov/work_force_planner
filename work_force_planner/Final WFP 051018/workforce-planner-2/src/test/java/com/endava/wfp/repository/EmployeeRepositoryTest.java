package com.endava.wfp.repository;

import com.endava.wfp.entity.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.endava.wfp.utility.Constants.TEST_ID;
import static org.junit.Assert.*;


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Checks if you can
     * save an employee in
     * the database and
     * find him after that
     */
    @Test
    public void saveAndRetrieveEmployeeTest() {

        Employee emp = new Employee();
        emp.setFirstName("Ivan");
        emp.setLastName("Petrov");
        emp.setEmail("ivanpetrov@gmail.com");
        emp.setPassword("password");
        emp.setManager(employeeRepository.findById(TEST_ID).get());
        Employee savedEmp = employeeRepository.save(emp);
        Optional<Employee> foundEmp = employeeRepository.findById(savedEmp.getId());

        assertNotNull(foundEmp);
        assertEquals(savedEmp.getEmail(), foundEmp.get().getEmail());

    }

    /**
     * Checks if you can
     * find an employee
     * by email and get the
     * first one found
     */
    @Test
    public void findFirstByEmailTest(){

        Employee emp1 = new Employee();
        emp1.setFirstName("Ivan");
        emp1.setLastName("Petrov");
        emp1.setEmail("hey@gmail.com");
        emp1.setPassword("password");
        emp1.setManager(employeeRepository.findById(TEST_ID).get());
        employeeRepository.save(emp1);

        assertEquals("Ivan", employeeRepository.findFirstByEmail("hey@gmail.com").get().getFirstName());

    }

    /**
     * Checks if an
     * employee with the
     * given e-mail exists
     */
    @Test
    public void existByEmailTest() {
        Employee emp1 = new Employee();
        emp1.setFirstName("Ivan");
        emp1.setLastName("Petrov");
        emp1.setEmail("hey@gmail.com");
        emp1.setPassword("password");
        emp1.setManager(employeeRepository.findById(TEST_ID).get());
        employeeRepository.save(emp1);

        assertTrue(employeeRepository.existsByEmailIgnoreCase(emp1.getEmail()));
    }

}

