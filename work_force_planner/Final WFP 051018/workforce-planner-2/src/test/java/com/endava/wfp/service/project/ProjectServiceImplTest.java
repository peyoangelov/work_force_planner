package com.endava.wfp.service.project;

import com.endava.wfp.dto.ProjectAddDTO;
import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.entity.Project;
import com.endava.wfp.repository.ProjectRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServiceImplTest {

    @Autowired
    private ProjectServiceImpl projectService;

    private ProjectAddDTO project;
    private Pageable pageableTest;
    private ProjectNameDTO projectNameDTOTest;
    private ModelMapper modelMapperTest;

    @Autowired
    private ProjectRepository projectRepositoryTest;

    /**
     * Checks if you
     * can find all projects
     */
    @Test
    public void findAllTest() {

        List<ProjectNameDTO> projects = this.projectService.findAll();
        assertNotNull(projects);
    }

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void addProjectSetup() {

        project = new ProjectAddDTO();
        project.setProjectName("Project");
        project.setStartDate(LocalDate.of(2018, 9, 20));
        project.setEndDate(LocalDate.of(2018, 12, 23));
        project.setDescription("This is a test project!");
    }

    /**
     * Test to see if adding
     * a project works
     * as intended
     */
    @Test
    public void addProject_successfullyTest() {

        Project projectTest = this.projectService.add(project);
        assertNotNull(projectTest);
        assertEquals(projectTest.getName(), project.getProjectName());
        assertEquals(projectTest.getStartDate(), project.getStartDate());
        assertEquals(projectTest.getEndDate(), project.getEndDate());
        assertEquals(projectTest.getDescription(), project.getDescription());
    }
}