package com.endava.wfp.service;

import com.endava.wfp.dto.CalculationsDTO;
import com.endava.wfp.dto.DistributionDTO;
import com.endava.wfp.entity.EmployeePosition;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.EmployeePositionRepository;
import com.endava.wfp.service.position.EmployeePositionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for all methods
 * in EmployeePositionService
 * which are used in
 * Allocation Status' page
 */
@RunWith(MockitoJUnitRunner.class)
public class EmployeePositionServiceTest {

    private EmployeePosition employeePosition;
    private Status status = Status.BOOKED;
    private CalculationsDTO calculationsDTO;
    private List<DistributionDTO> distributionDTOList;

    @Mock
    private EmployeePositionRepository employeePositionRepository;

    @InjectMocks
    private EmployeePositionServiceImpl employeePositionService;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setUp(){
        employeePosition = new EmployeePosition();
        employeePosition.setStatus(Status.BOOKED);

        employeePosition.setDiscipline(Discipline.JAVA);

        calculationsDTO = new CalculationsDTO();
        calculationsDTO.setCountOfEmployees(1L);
        calculationsDTO.setPercentageOfTotal(new BigDecimal("100.00"));

        distributionDTOList = new ArrayList<>();
        distributionDTOList.add(new DistributionDTO(Discipline.JAVA, 1L));
        distributionDTOList.add(new DistributionDTO(Discipline.dotNET, 5L));
        distributionDTOList.add(new DistributionDTO(Discipline.QA, 2L));
    }

    /**
     * Tests if
     * the count by status
     * is correct
     */
    @Test
    public void test_numberByStatus(){

        when(employeePositionRepository.countByStatus(status)).thenReturn(1L);

        assertThat(employeePositionService.numberByStatus(status)).isEqualTo(1L);

        verify(employeePositionRepository).countByStatus(status);
    }

    /**
     * Tests if
     * the count by status
     * is correct
     */
    @Test
    public void test_numberByStatusAndDiscipline(){

        when(employeePositionRepository.countByStatusAndDiscipline(status, Discipline.JAVA)).thenReturn(1L);

        assertThat(employeePositionService.numberByStatusAndDiscipline(status, Discipline.JAVA)).isEqualTo(1L);

        verify(employeePositionRepository).countByStatusAndDiscipline(status, Discipline.JAVA);

    }

    /**
     * Test if
     * the total
     * and
     * the percentage
     *
     */
    @Test
    public void test_calculateTotalAndPercentage(){

        when(employeePositionRepository.countByStatus(status)).thenReturn(1L);
        when(employeePositionRepository.count()).thenReturn(1L);

        assertThat(employeePositionService.calculateTotalAndPercentage(status)).isEqualTo(calculationsDTO);

        verify(employeePositionRepository).countByStatus(status);
    }

    /**
     * Test
     * the count
     * by discipline
     *
     */
    @Test
    public void test_calculateCountByDiscipline(){

        when(employeePositionRepository.countByStatusAndDiscipline(status, Discipline.JAVA)).thenReturn(1L);
        when(employeePositionRepository.countByStatusAndDiscipline(status, Discipline.dotNET)).thenReturn(5L);
        when(employeePositionRepository.countByStatusAndDiscipline(status, Discipline.QA)).thenReturn(2L);

        assertThat(employeePositionService.calculateCountByDiscipline(status)).isEqualTo(distributionDTOList);

        verify(employeePositionRepository).countByStatusAndDiscipline(status, Discipline.JAVA);
        verify(employeePositionRepository).countByStatusAndDiscipline(status, Discipline.dotNET);
        verify(employeePositionRepository).countByStatusAndDiscipline(status, Discipline.QA);
    }
}
