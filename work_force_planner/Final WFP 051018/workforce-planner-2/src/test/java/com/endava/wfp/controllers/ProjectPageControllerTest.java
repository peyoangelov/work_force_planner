package com.endava.wfp.controllers;

import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.service.project.ProjectService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@AutoConfigureMockMvc
@ContextConfiguration
public class ProjectPageControllerTest {

    @Mock
    private ProjectService projectService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser
    public void projectPage_hasAllAttributes() throws Exception{

        this.mockMvc.perform(get("/projects/{page}", 1))
                .andExpect(status().isOk())
                .andExpect(model()
                        .attributeExists("pageCreator", "pageNumber", "maxPages"))
                .andReturn();
    }

    @Test
    @WithMockUser
    public void getProjectsPage_test() throws Exception {
        this.mockMvc.perform(get("/projects/{page}", 1))
                .andExpect(status().isOk())
                .andExpect(view().name("project/project"))
                .andReturn();
    }

    @Test
    @WithMockUser
    public void getProjectsPage_redirectToSelectedEnteredPage() throws Exception {

        List<ProjectNameDTO> projectsList = projectService.findAll();

        this.mockMvc.perform(get("/projects/{page}?selectedPage={selectedPage}", 1, projectsList.size()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/projects/" + (projectsList.size())))
                .andReturn();
    }
}