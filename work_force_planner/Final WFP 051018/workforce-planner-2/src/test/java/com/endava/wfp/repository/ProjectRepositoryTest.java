package com.endava.wfp.repository;


import com.endava.wfp.entity.Project;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectRepositoryTest {

    @Autowired
    private ProjectRepository projectRepository;

    private Project setupProject;

    /**
     * Function for setting up
     * data before the tests
     * which is needed for the
     * Test methods
     */
    @Before
    public void findByName_createProjectSetup(){

        setupProject = new Project();
        setupProject.setName("TestProject");
        setupProject.setDescription("This is a test!");
        setupProject.setStartDate(LocalDate.of(2018,9,20));
        setupProject.setEndDate(LocalDate.of(2018,12,22));
    }
    /**
     *Checks if we have
     * found a project and if
     *the project which
     * was found
     * has the correct name
     */
    @Test
    public void findByName_returnSingleProjectTest(){

        Project projectTest = projectRepository.save(setupProject);
        assertNotNull(projectTest);
        assertEquals(projectTest,projectRepository.findByName("TestProject"));
    }

    /**
     * Checks if we
     * find all projects
     */
    @Test
    public void findAllProjectsTest(){

        List<Project> allProjectsTest = projectRepository.findAll();
        assertNotNull(allProjectsTest);
    }
}
