package com.endava.wfp.service.user;

import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.entity.*;
import com.endava.wfp.repository.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {

    @Mock
    private Skill skill;

    @Mock
    private EmployeePosition employeePosition;

    @Mock
    private Project project;

    @Mock
    private EmployeeProject employeeProject;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    private SkillRepository skillRepository;

    @Mock
    private PositionRepository positionRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private EmployeeAddDTO employeeAddDto;

    @Mock
    private Employee employee;

    @Mock
    private EmployeeProjectRepository employeeProjectRepository;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        when(employee.getManager()).thenReturn(employee);
        when(employee.getEmail()).thenReturn("test@abv.bg");
        when(employee.getPassword()).thenReturn("password");
        when(employee.getId()).thenReturn(1L);
        when(employeeRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(employee));
    }

    /**
     *
     * Test
     * to find
     * employee by id
     *
     */
    @Test
    public void test_findById_employee_successfully() {
        when(employeeRepository.findById(1L)).thenReturn(java.util.Optional.ofNullable(employee));
        assertEquals("Employee was found successfully!", employeeService.findById(1L), employee);
    }

    /**
     *
     * Test
     * loading the user
     * by his username
     */
    @Test
    public void test_load_user_by_username() {
        when(employeeRepository.findFirstByEmail("georgi@abv.bg")).thenReturn(java.util.Optional.ofNullable(employee));
        assertEquals("Employee was found correctly!", employeeService.loadUserByUsername("georgi@abv.bg"),
                new User("test@abv.bg", "1234", new HashSet<>()));
    }


    /**
     *
     * Test
     * the current
     * user details service
     */
    @Test
    public void test_current_UserDetails_Method() {
        thrown.expect(IllegalStateException.class);
        when(EmployeeServiceImpl.currentUserDetails()).thenThrow(new IllegalStateException());
        EmployeeServiceImpl.currentUserDetails();
    }
}