package com.endava.wfp.service.skill;

import com.endava.wfp.dto.SkillDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(SpringRunner.class)
@SpringBootTest
public class SkillServiceImplTest {

    @Autowired
    private SkillServiceImpl skillService;

    /**
     * Checks if
     * you can find all
     * employee_skills
     */
    @Test
    public void findAllTest() {
        List<SkillDTO> list = this.skillService.findAll();
        assertNotNull(list);
    }
}