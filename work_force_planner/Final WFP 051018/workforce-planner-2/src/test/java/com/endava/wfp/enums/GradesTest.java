package com.endava.wfp.enums;

import org.junit.Assert;
import org.junit.Test;

public class GradesTest {

    @Test
    public void fromStringJuniorTest() {

        String juniorValue = "Junior";
        String juniorName = "JUNIOR";

        Assert.assertEquals(juniorValue, Grade.fromString(juniorName).getValue());
        Assert.assertEquals(juniorName, Grade.JUNIOR.name());
    }

    @Test
    public void fromStringMidTest() {

        String midValue = "Mid";
        String midName = "MID";

        Assert.assertEquals(midValue, Grade.fromString(midName).getValue());
        Assert.assertEquals(midName, Grade.MID.name());
    }

    @Test
    public void fromStringSeniorTest() {

        String seniorValue = "Senior";
        String seniorName = "SENIOR";

        Assert.assertEquals(seniorValue, Grade.fromString(seniorName).getValue());
        Assert.assertEquals(seniorName, Grade.SENIOR.name());
    }
}
