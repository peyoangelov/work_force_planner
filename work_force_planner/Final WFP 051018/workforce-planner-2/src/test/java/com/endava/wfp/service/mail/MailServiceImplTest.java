package com.endava.wfp.service.mail;

import com.endava.wfp.enums.MailType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.context.Context;

import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class MailServiceImplTest {

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private MailServiceImpl mailServiceImpl;

    /**
     * Tests JavaMailSender.send() parameters
     */
    @Test
    public void sendMailVerified() {

        MimeMessagePreparator preparator = message -> {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED, "UTF-8");
        };

        this.javaMailSender = mock(JavaMailSender.class);
        doNothing().when(this.javaMailSender).send(isA(MimeMessagePreparator.class));
        this.javaMailSender.send(preparator);
        verify(this.javaMailSender, times(1)).send(preparator);
    }

    /**
     *  tests method sendMail()
     * @return false if JavaMailSender.send()
     * results in MailException
     */
    @Test
    public void sendMailReturnsFalseIfWrongRecipient() {
        Context context = new Context();
        assertFalse(this.mailServiceImpl.sendMail(MailType.EMPLOYEE_ADDED, "testrecipient@testmail.com", context));
    }
}