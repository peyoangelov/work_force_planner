#!/bin/sh
echo "********************************************************"
echo "Starting Workforce planner"
echo "Using Profile: $PROFILE"
echo "********************************************************"
java -Djava.security.egd=file:/dev/./urandom  \
     -Dspring.profiles.active=$PROFILE                          \
     -jar /usr/local/workforce-planner/workforce-planner.jar
