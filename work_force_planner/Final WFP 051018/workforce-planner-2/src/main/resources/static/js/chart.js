//create the chart object with preconfigured settings
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
   "valueField": "count",
  "titleField": "discipline",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "angle": 30,
  "labelText": "[[title]]: [[value]] employee(s)",
  "balloonText": "([[percents]]%)",
  "fontSize": 14,
  "labelRadius": 35,
  "export": {
    "enabled": true
  },
  "startEffect": "bounce"
} );

$(document).ready(function() {
     // call the ajax, when page is ready, to fill the Total and percentages for the first status
     callAjaxToFillTotal('Booked');
     // call the ajax, when page is ready, to fill the data for the chart for the first status
     callAjaxToFillChart('Booked');
     // on each button click, manage the css and call ajax to render correct data
    $('.menu-button').on('click', function(e){
        $('.menu-button').removeClass('active');
        $(e.target).addClass('active');
        var selectedStatus = $(e.target).attr('data-status-code');
        callAjaxToFillTotal(selectedStatus);
        callAjaxToFillChart(selectedStatus);
        var activeButton = $('button.active').html();
        $('[data-allocation-status]').html(activeButton)
    })
})

function callAjaxToFillTotal(status){
    $.ajax({
        url : '/getAllocationDataForTotal?status='+status,
        type : 'GET',
        dataType: "json",
        success : function(data) {
              $('[data-total-number]').html(data.countOfEmployees);
              $('[data-total-percentage]').html(data.percentageOfTotal);
            }
    });
}

function callAjaxToFillChart(status){
    $.ajax({
        url : '/getAllocationDataForChart?status='+status,
        type : 'GET',
        dataType: "json",
        success : function(data) {
            var chartData=[];
            // set the newly revived data to the chart
            $.each(data ,function(index, element){
               //renaming dotNET to .NET
                if( element.discipline.toLowerCase().indexOf('net') >= 0){
                   element.discipline = element.discipline.replace('dot', '.');
                 }
                  chartData.push(element);
            })
            chart.dataProvider = chartData;
            //after calling this method the chart will parse data and redraw
            chart.validateData();
            chart.animateAgain();
        }
    });

}
