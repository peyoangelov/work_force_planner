function changeFunc() {
     var project = $('#project');
     var statusValue = $('#status').val();
     if(statusValue === 'Current bench') {
         var option = $('#project option[name^="Current"]')
         option.remove();
         project.append(option);
         var options = $('#project option').get();
         project.append(options);
         project.prop('disabled', true);
     } else {
         project.removeAttr('disabled');
     }
}