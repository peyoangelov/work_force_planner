// Add active class to the current button (highlight it)
var header = document.getElementById("managementTabs");
var btns = header.getElementsByClassName("btn-tab");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("tab-active");
    current[0].className = current[0].className.replace(" tab-active", "");
    this.className += " tab-active";
  });
}