    $(document).ready(function() {
    let checkboxes = Array.from($('.checkbox'));
    let isSmthChecked = 0;
    let submitBtn = document.getElementById("submitButton");

    for(let element of checkboxes){
      $(element).on('change', function(event){
         const startDate = $($(element).parent().parent().children()[4]).children()[0];
         const endDate = $($(element).parent().parent().children()[5]).children()[0];
            if(event.target.checked){
                startDate.disabled = false;
                startDate.required = true;
                endDate.disabled = false;
                endDate.required=true;
                isSmthChecked++;

            }else{
                startDate.disabled = true;
                endDate.disabled = true;
                isSmthChecked--;

            }

            if(isSmthChecked > 0){
                submitBtn.disabled = false;
            } else {
                submitBtn.disabled = true;
            }


           }
        )

    }

    var ul = document.getElementById('pagination');
        ul.onclick = function(event) {
            var target = event.target;
                for(let element of checkboxes){
                    if($(element).is(":checked")){
                        var confirm = window.confirm("You have unsubmitted changes. They will be discarded if you navigate away from this page. \n\n Press OK to continue, or Cancel to stay on the current page.");

                         if(confirm){
                              window.location.replace(event.target.href);
                              break;
                          }else {
                               event.preventDefault();
                               break;
                          }
                                  break;
                    }

                }

                };
});