package com.endava.wfp.dto;

import com.endava.wfp.enums.RequestStatus;
import com.endava.wfp.utility.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestDTO {

    private Long id;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100)
    private String firstName;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100)
    private String lastName;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100)
    private String email;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 300, message = Constants.REASON_FOR_REQUEST_MESSAGE)
    private String reasonForRequest;

    private LocalDateTime requestDate;

    private RequestStatus status;
}
