package com.endava.wfp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Configuration
public class GitPropertiesConfig {

    private static final String COMMIT_ID_PATTERN = "git\\.commit\\.id\\.abbrev=[a-z0-9]*";
    private static final String BUILD_VERSION_PATTERN = "git\\.build\\.version=[a-zA-Z0-9.\\-]*";
    private static final String GIT_PROPERTIES_FILENAME = "git-commit-id.properties";

    /**
     * reads git properties
     * from git-commit-id.properties
     * file
     *
     * @return git.commit.id
     * and build.version as String
     */
    @Bean(name = "gitProperties")
    public String readGitProperties() {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(GIT_PROPERTIES_FILENAME);
        try {
            return readFromInputStream(inputStream);
        } catch (IOException | NullPointerException e) {
            return "Version information could not be retrieved";
        }
    }

    /**
     * reading and
     * formatting String
     * from InputStream
     *
     * @param inputStream
     * @return appended build.version
     * and git.commit.id
     * as String with '-'
     * @throws IOException
     */
    public String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();


        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            while ((line = br.readLine()) != null) {
                if(line.matches(BUILD_VERSION_PATTERN)) {
                    resultStringBuilder.append("v").append(line.split("=")[1]).append('-');
                }
                else if(line.matches(COMMIT_ID_PATTERN)) {
                    resultStringBuilder.append(line.split("=")[1]);
                }
            }
        }

        return resultStringBuilder.toString();
    }
}
