package com.endava.wfp.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Entity corresponding to
 * table employee and its
 * relationships in the
 * database
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"subordinates", "employeesPositions", "employeesProjects", "manager"})
@EqualsAndHashCode(exclude = {"subordinates", "employeesPositions", "employeesProjects", "manager"})
@Entity
@Table(name = "employees")
public class Employee {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 100)
    @Column(name = "last_name")
    private String lastName;

    @Size(max = 100)
    @Column(name = "email", unique = true)
    @NotNull(message = "The email must not be empty!")
    private String email;

    @Size(min = 4, max = 100, message = "The password's length must be between 4 and 8!")
    @Column(name = "password")
    private String password;

    //Manager relation
    @ManyToOne()
    @JoinColumn(name = "manager_id")
    private Employee manager;

    @OneToMany(mappedBy = "manager")
    private List<Employee> subordinates;
    //Manager relation

    //Employees_Positions relation
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employee", fetch = FetchType.EAGER)
    private EmployeePosition employeesPositions;
    //Employees_Positions relation

    //Employees_Projects relation
    @OneToMany(mappedBy = "employee", orphanRemoval = true)
    private List<EmployeeProject> employeesProjects;
    //Employees_Projects relation
}