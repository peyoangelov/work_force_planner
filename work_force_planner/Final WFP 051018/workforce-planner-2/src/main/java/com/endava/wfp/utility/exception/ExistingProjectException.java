package com.endava.wfp.utility.exception;

/**
 * Exception class
 * for validation if
 * a project with the name
 * requested by the manager
 * already exists if it does
 * this custom exception is thrown
 */
public class ExistingProjectException extends RuntimeException {
    public ExistingProjectException(String message) {
        super(message);
    }
}
