package com.endava.wfp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class DashboardController {

    /**
     * Displaying the dashboard
     *
     * @return sends the
     *         user to
     *         the dashboard
     */
    @GetMapping("/dashboard")
    public String displayDashboard() {
        log.info("Displaying the dashboard");
        return "dashboard";
    }
    /**
     * Displaying the navigation bar
     *
     * @return displays nav bar
     *         in dashboard
     */
    @GetMapping("/nav")
    public String displayNavigation() {
        log.info("Displaying the navigation bar");
        return "navigation";
    }
}
