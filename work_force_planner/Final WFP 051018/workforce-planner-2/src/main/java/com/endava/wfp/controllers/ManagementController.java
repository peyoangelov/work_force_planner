package com.endava.wfp.controllers;

import com.endava.wfp.dto.ManagementDTO;
import com.endava.wfp.model.PageCreator;
import com.endava.wfp.service.management.ManagementService;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ManagementController {

    private final ManagementService managementService;
    private final EmployeePagination employeePagination;
    private static final String REDIRECT_PAGE_MANAGEMENT = "management";

    /**
     *
     * @param page the current page
     *             in list of employees
     *
     * @param selectedPage the current sorting
     *                     property in list of employees
     *
     * @return redirects to list of employees
     */
    @GetMapping("/management/{page}")
    public ModelAndView management(@PathVariable("page") Integer page, @RequestParam(value = "selectedPage", required = false) Integer selectedPage) {
        ModelAndView modelAndView = new ModelAndView("management/management");

        if (page < Constants.FIRST_PAGE) {
            page = Constants.FIRST_PAGE;
        }

        int pageIndex = page - 1;

        Page<ManagementDTO> managementList = managementService.findAll(PageRequest.of(pageIndex, Constants.EMPLOYEE_PER_PAGE));

        PageCreator pageCreator = new PageCreator(managementList.getTotalPages(), managementList.getNumber());

        modelAndView.addObject("managementList", managementList);
        modelAndView.addObject("pageCreator", pageCreator);
        modelAndView.addObject("pageNumber", managementList.getNumber());
        modelAndView.addObject("maxPages", managementList.getTotalPages());

        /**
         * Pagination select input logic
         */
        if (selectedPage != null) {
            modelAndView = employeePagination.redirectSearchPage(managementList, selectedPage, REDIRECT_PAGE_MANAGEMENT);
        }
        log.info("Open management page {}", managementList.getNumber());
        return modelAndView;
    }
}