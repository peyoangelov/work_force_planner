package com.endava.wfp.scheduler;

import com.endava.wfp.entity.Request;
import com.endava.wfp.enums.RequestStatus;
import com.endava.wfp.repository.RequestRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class RequestAccessScheduler {

    private final RequestRepository requestRepository;

    public RequestAccessScheduler (RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    /**
     * Every day it has to check all submitted requests in the db
     * and reject all not approved requests.
     * A temp file is created with info for the automatically rejected requests.
     * Cron expression is set in application.yml
     * @throws IOException
     */
    @Scheduled(cron = "${cron.schedulerTime}")
    public void reportRequests () throws IOException {

        File reportFile = File.createTempFile(LocalDate.now().toString(), ".txt");
        try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(reportFile)))) {
            //retrieve all pending requests
            List<Request> requests = this.requestRepository.findByStatus(RequestStatus.PENDING);
            //for each request - change it status, save it to db and write it to the file
            for (Request request : requests) {
                request.setStatus(RequestStatus.REJECTED);
                this.requestRepository.save(request);
                //used printf to format the ouput in the report file
                writer.printf("First name: %s%n Last name: %s%n E-mail: %s%n Reason for request: %s%n Request Date: %tD%n Current date: %tD%n  ",
                        request.getFirstName(), request.getLastName(), request.getEmail(), request.getReasonForRequest(), request.getRequestDate(), LocalDateTime.now());
            }
        }
    }
}
