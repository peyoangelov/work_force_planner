package com.endava.wfp.view;

import com.endava.wfp.dto.EmployeePageDTO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class ExcelExportView extends AbstractXlsxView {
    private final String[] columns = {"Name", "Email", "Discipline", "Allocation Status", "Skills"};

    /**
     * This method builds excel document based on list of EmployeePageDTOs.
     * The document is then send for download from the browser.
     * @param map contains the list of EmployeePageDTOs
     * @param workbook the excel document to be built
     * @param request
     * @param response
     * @throws Exception
     * @see EmployeePageDTO
     */
    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Disposition", "attachment;filename=\"page_export.xlsx\"");
        List<EmployeePageDTO> employees = (List<EmployeePageDTO>) map.get("listOfEmployees");
        int rowNum = 0;

        Sheet employeeSheet = workbook.createSheet("Employees");
        Row headerRow = employeeSheet.createRow(rowNum);

        fillRow(headerRow, columns);

        for (EmployeePageDTO employee : employees) {
            rowNum++;
            Row row = employeeSheet.createRow(rowNum);
            fillRow(row, employee.toStringArray());
        }
    }

    /**
     * This method fills a row in excel sheet with given values
     * @param row the row to be filled
     * @param values values to be inserted into the row
     */
    private void fillRow(Row row, String[] values) {
        IntStream.range(0, columns.length).forEach(index -> {
            Cell cell = row.createCell(index);
            cell.setCellValue(values[index]);
        });
    }
}
