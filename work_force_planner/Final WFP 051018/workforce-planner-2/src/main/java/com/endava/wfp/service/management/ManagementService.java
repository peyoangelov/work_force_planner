package com.endava.wfp.service.management;

import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.dto.ManagementDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.EmployeeHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Interface for ManagementServiceImpl class for override methods
 */
public interface ManagementService {
    Page<ManagementDTO> findAll(Pageable pageable);
    EmployeeHistory deleteEmployeeAudit(Employee employee);
    EmployeeHistory saveHistoryAudit(Employee employee);
    EmployeeHistory editEmployeeAudit(Employee currentUser, EmployeeEditDTO dto);
}
