package com.endava.wfp.dto;

import com.endava.wfp.dto.interfaces.IStartEndDateForm;
import com.endava.wfp.dto.interfaces.IStatusProjectForm;
import com.endava.wfp.utility.Constants;
import com.endava.wfp.utility.validations.StartEndDateConstraint;
import com.endava.wfp.utility.validations.StatusConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

import static com.endava.wfp.utility.Constants.*;

/**
 * Dto used for
 * adding a new employee
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@StartEndDateConstraint //StartDate and EndDate validation
@StatusConstraint //Status and Project validation
public class EmployeeAddDTO implements IStartEndDateForm, IStatusProjectForm {

    public static final String PROJECT_SIZE_MESSAGE = "Project must be between 1 and 200 characters";
    public static final String PASSWORD_SIZE_MESSAGE = "Password must be between 4 and 100 characters";
    public static final String MANAGER_ID_SIZE_MESSAGE = "Manager Id should not be less than 1";
    public static final String GRADE_SIZE_MESSAGE = "Grade must be between 1 and 200 characters";
    public static final String SKILL_SIZE_MESSAGE = "Skill must be between 1 and 200 characters";
    public static final String STATUS_SIZE_MESSAGE = "Status must be between 1 and 200 characters";

    @NotNull(message = BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100, message = E_MAIL_SIZE_MESSAGE)
    @Email
    private String email;

    @NotNull(message = BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 4, max = 100, message
            = PASSWORD_SIZE_MESSAGE)
    private String password;

    @NotNull(message = BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100, message
            = FIRST_NAME_SIZE_MESSAGE)
    private String firstName;

    @NotNull(message = BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100, message
            = LAST_NAME_SIZE_MESSAGE)
    private String lastName;

    @NotNull(message = BLANK_FIELD)
    @Min(value = 1, message = MANAGER_ID_SIZE_MESSAGE)
    private Long managerId;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 200, message
            = GRADE_SIZE_MESSAGE)
    private String grade;

    @NotNull
    @NotBlank
    private String discipline;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 200, message
            = STATUS_SIZE_MESSAGE)
    private String status;

    @NotNull
    @Size(min = 1, max = 200, message
            = PROJECT_SIZE_MESSAGE)
    private String project="Current Bench";

    @NotNull
    @NotBlank
    @Size(min = 1, max = 200, message
            = SKILL_SIZE_MESSAGE)
    private String skill;

    @NotNull(message = BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
}
