package com.endava.wfp.controllers;

import com.endava.wfp.dto.ProjectEmployeeDTO;
import com.endava.wfp.entity.Project;
import com.endava.wfp.model.PageCreator;
import com.endava.wfp.repository.ProjectRepository;
import com.endava.wfp.service.employee_project.EmployeeProjectService;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectListEmployeesController {

    private final ProjectRepository projectRepository;
    private final EmployeeProjectService employeeProjectService;
    private final EmployeePagination employeePagination;

    /**
     * .
     *
     * @param projectName  the name of
     *                     the project
     *                     taken from
     *                     the html
     * @param page         the page at which
     *                     the user is currently
     * @param selectedPage the page selected
     *                     in the Go to page
     *                     field
     * @return the view
     */
    @GetMapping("projects/{projectsPage}/employeesList/{projectName}/{page}")
    public ModelAndView projectEmployeeList(@PathVariable("projectName") String projectName,
                                            @PathVariable("page") Integer page,
                                            @PathVariable("projectsPage") Integer projectsPage,
                                            @RequestParam(value = "selectedPage", required = false) Integer selectedPage) {

        ModelAndView model = new ModelAndView("project/viewProjectPage.html");

        final String projectString = "project";
        final String REDIRECT_PAGE_LIST_EMPLOYEES_ON_PROJECT = "projects/" + projectsPage + "/employeesList";

        Project project = projectRepository.findByName(projectName);
        model.addObject(projectString, project);

        if (page < Constants.FIRST_PAGE) {
            page = Constants.FIRST_PAGE;
        }

        int pageIndex = page - 1;

        Page<ProjectEmployeeDTO> allEmployeesOnProject = employeeProjectService.findAllByProject(project.getId(),
                        PageRequest.of(pageIndex, Constants.EMPLOYEE_PER_PAGE, Sort.by("id").descending()));
        /**
         * the count of page
         * is starting with zero,
         * that's why we
         * add + 1
         * in order to make
         * the page
         * count correct
         */
        PageCreator pageCreator = new PageCreator(allEmployeesOnProject.getTotalPages(), allEmployeesOnProject.getNumber());

        model.addObject("allEmployeesOnProjectOnPage", allEmployeesOnProject);
        model.addObject("pageCreator", pageCreator);
        model.addObject("pageNumber", allEmployeesOnProject.getNumber());
        model.addObject("maxPages", allEmployeesOnProject.getTotalPages());
        model.addObject("projectsPage", projectsPage);

        if (selectedPage != null) {
            model = employeePagination.redirectSearchPageByProject(allEmployeesOnProject,
                    selectedPage, REDIRECT_PAGE_LIST_EMPLOYEES_ON_PROJECT, project.getName());
        }

        if (allEmployeesOnProject.getTotalPages() == 0) {
            return model;
        } else if (!allEmployeesOnProject.hasContent()) {
            return new ModelAndView("redirect:/projects/" + projectsPage + "/employeesList/" + projectName + "/" + (page - 1));
        }
        return model;
    }

    /**
     * Remove Employee from the Project
     * @param page receive page number
     * @param projectName Name of the project
     * @param projectsPage page of the project
     * @param employeeId id of Employee
     * @return ModelAndView and redirect to specific page
     */
    @PostMapping("projects/{projectsPage}/employeesList/{projectName}/{employeeId}/{page}/remove")
    public ModelAndView removeEmployeeFromProject(@PathVariable("page") Long page,
                                                  @PathVariable("projectName") String projectName,
                                                  @PathVariable("projectsPage") Integer projectsPage,
                                                  @PathVariable("employeeId") Long employeeId) {

        this.employeeProjectService.delete(employeeId, projectRepository.findByName(projectName).getId());

        return new ModelAndView("redirect:/projects/" + projectsPage + "/employeesList/" + projectName + "/" + page);
    }

}
