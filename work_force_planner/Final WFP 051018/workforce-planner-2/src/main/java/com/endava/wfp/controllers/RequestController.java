package com.endava.wfp.controllers;

import com.endava.wfp.dto.RequestDTO;
import com.endava.wfp.service.request.RequestAccessService;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class RequestController {

    private final RequestAccessService requestAccessService;

    /**
     * Open request page
     *
     * @param model
     *
     * @return
     */
    @GetMapping("/request")
    public ModelAndView request(Model model) {
        log.info("Display request page with all requests.");
        List<RequestDTO> requestDTOs = this.requestAccessService.findByStatus(Constants.STATUS_PENDING);
        model.addAttribute("list", requestDTOs);
        return new ModelAndView("management/request");
    }
    /**
     * Getting status and id from
     * request screen hidden fields
     * on press of buttons rejected
     * and approved
     *
     * @param status
     *
     * @param id
     *
     * @return
     */
    @PostMapping("/request")
    public ModelAndView request(String status, Long id) {
        log.info("get status for rejected and approved");
        this.requestAccessService.requestStatus(status,id);
        return new ModelAndView("redirect:request");
    }
}
