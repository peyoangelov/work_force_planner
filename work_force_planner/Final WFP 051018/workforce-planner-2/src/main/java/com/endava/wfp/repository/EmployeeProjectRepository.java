package com.endava.wfp.repository;

import com.endava.wfp.entity.EmployeeProject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeProjectRepository extends JpaRepository<EmployeeProject, Long> {

    /**
     *
     * @param employeeId the id of the
     *                   employee designated
     *                   to be deleted
     * @param projectId
     * Deletes a sample
     * with the indicated
     * employeeId
     */
    void deleteByEmployeeIdAndProjectId(Long employeeId, Long projectId);

    /**
     *
     * @param projectId id of the
     *                  selected project
     * @param pageable variable for
     *                 pagination
     * @return pages of
     * employeeProjects
     */
    Page<EmployeeProject> findAllByProjectId(Long projectId, Pageable pageable);

}
