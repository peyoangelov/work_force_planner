package com.endava.wfp.utility.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = StatusValidator.class)
@Documented
@Target(value = ElementType.TYPE)
@Retention(value = RUNTIME)
public @interface StatusConstraint {
    String message() default "Project cannot be 'Current Bench'!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
