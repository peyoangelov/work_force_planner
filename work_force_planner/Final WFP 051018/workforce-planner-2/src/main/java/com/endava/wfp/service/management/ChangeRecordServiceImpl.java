package com.endava.wfp.service.management;

import com.endava.wfp.entity.ChangeRecord;
import com.endava.wfp.repository.ChangeRecordRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ChangeRecordServiceImpl implements ChangeRecordService {
    private final ChangeRecordRepository changeRecordRepository;

    /**
     * Accept list with records and save them in DB
     * @param changeRecords receive list with Records
     * @return list saved in DB records
     */
    @Override
    public List<ChangeRecord> saveAll(List<ChangeRecord> changeRecords) {
        log.info("Save all change records in db");
        return this.changeRecordRepository.saveAll(changeRecords);
    }
}
