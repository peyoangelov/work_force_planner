package com.endava.wfp.enums;

public enum Discipline {

    JAVA("Java"),
    dotNET("dotNET"),
    QA("QA");

    private String value;

    Discipline(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Discipline fromString(String name) {
        for(Discipline d : Discipline.values()) {
            if(d.value.equalsIgnoreCase(name)) {
                return d;
            }
        }
        throw new IllegalArgumentException("No constant with text " + name + " found");
    }
}