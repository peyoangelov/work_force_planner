package com.endava.wfp.entity;

import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.enums.Status;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Entity corresponding to
 * table employees_position with
 * its relationships in the
 * database
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "employee")
@EqualsAndHashCode(exclude = "employee")
@Entity
@Table(name = "employees_positions")
public class EmployeePosition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "grade")
    @Enumerated(EnumType.STRING)
    private Grade grade;

    @Column(name = "discipline")
    @Enumerated(EnumType.STRING)
    private Discipline discipline;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    //Employee relation
    @OneToOne
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;
    //Employee relation

    //Employees_Skills relation
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "employees_skills",
            joinColumns = @JoinColumn(name = "employee_position_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"))
    private List<Skill> skills;
    //Employees_Skills relation
}