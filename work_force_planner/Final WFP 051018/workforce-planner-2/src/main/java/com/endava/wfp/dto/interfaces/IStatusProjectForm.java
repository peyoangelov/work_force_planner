package com.endava.wfp.dto.interfaces;
/**
 *Interface used for
 *Status and Project
 *validation
 *(Project cannot be current bench
 *when status is not current bench)
 */
public interface IStatusProjectForm {

    String getProject();
    String getStatus();
}
