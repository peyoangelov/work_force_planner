package com.endava.wfp.dto;

import com.endava.wfp.utility.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * DTO used to
 * display all employees
 * currently on a given
 * project
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectEmployeeDTO {

    private Long id;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String name;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String discipline;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String grade;

    @NotNull(message = Constants.BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = Constants.BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
}
