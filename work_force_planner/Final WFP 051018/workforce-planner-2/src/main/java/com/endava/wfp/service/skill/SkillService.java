package com.endava.wfp.service.skill;

import com.endava.wfp.dto.SkillDTO;

import java.util.List;
/**
 * Interface for SkillServiceImpl class for override methods
 */
public interface SkillService {
    List<SkillDTO> findAll();
}
