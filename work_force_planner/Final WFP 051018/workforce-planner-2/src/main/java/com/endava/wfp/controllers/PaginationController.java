package com.endava.wfp.controllers;


import com.endava.wfp.dto.EmployeePageDTO;
import com.endava.wfp.model.PageCreator;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.utility.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
public class PaginationController {

    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String EMPLOYEES_POSITIONS_DISCIPLINE = "employeesPositionsDiscipline";
    private static final String EMPLOYEES_POSITIONS_STATUS = "employeesPositionsStatus";
    private static final String FORMATTED_NAME = "Name";
    private static final String FORMATTED_EMAIL = "E-mail";
    private static final String FORMATTED_DISCIPLINE = "Discipline";
    private static final String FORMATTED_STATUS = "Status";
    private static final String REDIRECT_PAGE_LIST = "list";

    @Autowired
    private EmployeePagination employeePagination;

    /**
     * Open page list of employees
     *
     * @param page next page in
     *             list of employees
     *
     *
     * @param sortingProperty current sorting property
     *                        in list of employees
     *
     * @param selectedPage current page
     *                     in list of employees
     *
     * @return
     */
    @GetMapping(value = "/list/{page}/{sortBy}")
    public ModelAndView listEmployees(@PathVariable("page") Integer page,
                                      @PathVariable(value = "sortBy", required = false) String sortingProperty,
                                      @RequestParam(value = "selectedPage", required = false) Integer selectedPage){

        log.info("Open list of employees");
        ModelAndView modelAndView = new ModelAndView("list");

        if (page < Constants.FIRST_PAGE) {
            page = Constants.FIRST_PAGE;
        }

        int evalPage = page - 1;
        Pageable pageable;

        pageable = PageRequest.of(evalPage, Constants.EMPLOYEE_PER_PAGE, Sort.by(sortingProperty.split(",")).ascending());
        Page<EmployeePageDTO> employeeList = employeePagination.findAll(pageable);
        /**
         * The count of page is stating with zero, that's why we add +1 in order to make the page count correct
         */
        PageCreator pageCreator = new PageCreator(employeeList.getTotalPages(), employeeList.getNumber() + 1);
        /**
         * Add Employee model
         */
        modelAndView.addObject("employeeList", employeeList);
        /**
         * Evaluate page size
         */
        modelAndView.addObject("pageCreator", pageCreator);
        modelAndView.addObject("pageNumber", employeeList.getNumber());
        modelAndView.addObject("sortByCode", sortingProperty);
        modelAndView.addObject("maxPages", employeeList.getTotalPages());
        /**
         * Based on the selected sort code,
         * add formatted name for the code to be displayed nicely
         */
        setSelectedSortName(modelAndView, sortingProperty);
        /**
         * Pagination select input logic
         */
        if (selectedPage != null) {
            modelAndView = employeePagination.redirectSearchPageBySortingProperty(employeeList,
                    selectedPage,
                    sortingProperty,
                    REDIRECT_PAGE_LIST);
        }

        if (!employeeList.hasContent()) {
            return new ModelAndView("redirect:/list/" + (page - 1) + "/firstName,lastName");
        } else {
            return modelAndView;
        }
    }
    /**
     * Select the sort name
     *
     * @param modelAndView model and view where
     *                     information is stored
     *
     * @param selectedCode sorting code
     */
    private void setSelectedSortName(final ModelAndView modelAndView, final String selectedCode) {

        String statusName;
        switch (selectedCode) {
            case NAME:
                statusName = FORMATTED_NAME;
                break;
            case EMAIL:
                statusName = FORMATTED_EMAIL;
                break;
            case EMPLOYEES_POSITIONS_DISCIPLINE:
                statusName = FORMATTED_DISCIPLINE;
                break;
            case EMPLOYEES_POSITIONS_STATUS:
                statusName = FORMATTED_STATUS;
                break;
            default:
                statusName = FORMATTED_NAME;
                break;
        }
        log.info("add the assigned name to the model");
        /**
         * Add the assigned name to the model,
         * so it can be used in the html
         */
        modelAndView.addObject("sortByName", statusName);
    }
}
