package com.endava.wfp.dto;

import lombok.Data;

/**
 * DTO used to get skills name
 */
@Data
public class SkillDTO {

    private String name;
}
