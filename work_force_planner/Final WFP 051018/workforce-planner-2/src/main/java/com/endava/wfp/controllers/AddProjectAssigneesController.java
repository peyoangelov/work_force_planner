package com.endava.wfp.controllers;

import com.endava.wfp.dto.ProjectAssigneesDTO;
import com.endava.wfp.enums.Status;
import com.endava.wfp.model.PageCreator;
import com.endava.wfp.repository.ProjectRepository;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.service.position.EmployeePositionService;
import com.endava.wfp.service.project.ProjectService;
import com.endava.wfp.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AddProjectAssigneesController {

    @Autowired
    private EmployeePositionService employeePositionService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private EmployeePagination employeePagination;

    @Autowired
    private ProjectRepository projectRepository;

    /**
     * @param filteringProperty filters the employees
     *                          by chosen status
     * @param page              corresponds to the page
     *                          we are on from the pagination
     * @param selectedPage      cooperates with text box
     *                          which can be filled
     *                          by the user and goes to the
     *                          chosen page
     * @return  information for part
     *          of the employees
     *          dependion on the status
     *          which was chosen
     *          on default they are
     *          filtered by status
     *          "Current Bench"
     */
    @GetMapping("/assign/{projectsPage}/{projectName}/{page}/{filterBy}")
    public ModelAndView assignEmployees(@PathVariable(value = "filterBy") Status filteringProperty,
                                        @PathVariable(value = "page") Integer page,
                                        @PathVariable(value = "projectsPage") Integer projectsPage,
                                        @RequestParam(value = "selectedPage", required = false) Integer selectedPage,
                                        @PathVariable(value = "projectName") String projectName) {

        ModelAndView modelAndView = new ModelAndView("project/assign");

        if (page < Constants.FIRST_PAGE) {
            page = Constants.FIRST_PAGE;
        }

        int pageIndex = page - 1;

        Page<ProjectAssigneesDTO> projectAssigneesList = employeePositionService.filterEmployeesByStatus(filteringProperty,
                PageRequest.of(pageIndex, Constants.EMPLOYEE_PER_PAGE, Sort.by("id").descending()));

        PageCreator pageCreator = new PageCreator(projectAssigneesList.getTotalPages(),
                projectAssigneesList.getNumber() + 1);

        modelAndView.addObject("filterByCode", filteringProperty);
        modelAndView.addObject("project", projectRepository.findByName(projectName));
        modelAndView.addObject("projectAssigneesDTOS", projectAssigneesList);
        modelAndView.addObject("pageCreator", pageCreator);
        modelAndView.addObject("pageNumber", projectAssigneesList.getNumber());
        modelAndView.addObject("maxPages", projectAssigneesList.getTotalPages());
        modelAndView.addObject("projectsPage",projectsPage);

        setSelectedSortName(modelAndView, filteringProperty);

        if (selectedPage != null) {
            modelAndView = employeePagination.redirectSearchPageByFilteringProperty(projectAssigneesList,projectsPage,
                    selectedPage, filteringProperty, projectName);
        }

        return modelAndView;
    }

    /**
     * @param ids        contains all employees' ids
     *                   according to the
     *                   selected checkboxes
     *                   by the user
     * @param startDates contains all employees'
     *                   start dates for working
     *                   on a project which were
     *                   filled in by the user
     * @param endDates   contains all employees'
     *                   end dates for working
     *                   on a project which were
     *                   filled in by the user
     * @return  the to either
     *          the project page
     *          or the current
     *          view with error message
     */
    @PostMapping("/assign/{projectsPage}/{projectName}/{page}/{filterBy}")
    public ModelAndView assignEmployees(@RequestParam(value = "dto") ArrayList<Long> ids,
                                        @RequestParam(value = "startDate") ArrayList<String> startDates,
                                        @RequestParam(value = "endDate") ArrayList<String> endDates,
                                        @PathVariable(value = "projectName")String projectName,
                                        @PathVariable(value = "projectsPage") Integer projectsPage) {
        try {
            projectService.addEmployeesToProject(ids, startDates, endDates, projectName);
        } catch (ValidationException e) {
            return new ModelAndView("redirect:/assign/{projectsPage}/{projectName}/{page}/{filterBy}" + "?error=true");
        }

        return new ModelAndView("redirect:/projects/{projectsPage}/employeesList/{projectName}/1");
    }

    /**
     * Set Sort Name
     * @param modelAndView for parameters
     * @param selectedCode receives Status
     */
    private void setSelectedSortName(final ModelAndView modelAndView, final Status selectedCode) {

        String statusName = "Current Bench";

        for (String status : allStatuses()) {
            if (selectedCode.getValue().equals(status)) {
                statusName = status;
                break;
            }
        }

        modelAndView.addObject("sortByName", statusName);
    }
    /**
     * @return list of all values
     * from the Status enum
     */
    private List<String> allStatuses() {
        return EnumSet
                .allOf(Status.class)
                .stream()
                .map(Status::getValue)
                .collect(Collectors.toList());
    }
}
