package com.endava.wfp.utility.exception;

public class StartAndEndDateException extends RuntimeException {
    public StartAndEndDateException(String errorMessage) {
        super(errorMessage);
    }
}
