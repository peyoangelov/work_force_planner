package com.endava.wfp.service.skill;

import com.endava.wfp.dto.SkillDTO;
import com.endava.wfp.repository.SkillRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SkillServiceImpl implements SkillService{

    private final SkillRepository skillRepository;
    private final ModelMapper modelMapper;

    /**
     * Find and return all Skills
     * @return list of SkillDTO
     */
    @Override
    public List<SkillDTO> findAll() {
        log.info("Find and return all Skills.");
        /**
         * Map Skills to SkillDTO
         */
        return this.skillRepository.findAll()
                .stream()
                .map(s -> this.modelMapper.map(s, SkillDTO.class)).collect(Collectors.toList());
    }
}
