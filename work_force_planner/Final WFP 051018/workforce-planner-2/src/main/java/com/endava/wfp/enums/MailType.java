package com.endava.wfp.enums;

public enum MailType {

    EMPLOYEE_ADDED("Employee added"),
    PROJECT_ASSIGNED("Project assigned"),
    EMPLOYEE_ASSIGNED("Employee assigned to project");

    private String value;

    MailType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
