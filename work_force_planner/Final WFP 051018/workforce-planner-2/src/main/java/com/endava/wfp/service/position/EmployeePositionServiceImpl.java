package com.endava.wfp.service.position;

import com.endava.wfp.dto.CalculationsDTO;
import com.endava.wfp.dto.DistributionDTO;
import com.endava.wfp.dto.ProjectAssigneesDTO;
import com.endava.wfp.entity.EmployeePosition;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.EmployeePositionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeePositionServiceImpl implements EmployeePositionService {

    private final EmployeePositionRepository employeePositionRepository;
    private final ModelMapper modelMapper;

    /**
     * Accept Status and return number EmployeePositions
     * @param status receives Status
     * @return number of Statuses
     */
    @Override
    public Long numberByStatus(Status status) {
        log.info("Accept Status and return number EmployeePositions.");
        return employeePositionRepository.countByStatus(status);
    }
    /**
     * Accept Status and Discipline
     * and return count of EmployeePositions
     * @param status receives Status
     * @param discipline receives Discipline
     * @return number of Statuses and Disciplines
     */
    @Override
    public Long numberByStatusAndDiscipline(Status status, Discipline discipline) {
        log.info("Accept Status and Discipline and return count of EmployeePositions.");
        return employeePositionRepository.countByStatusAndDiscipline(status, discipline);
    }
    /**
     * Accept Status and return calculation
     * and percentage for employee positions
     * @param status receives Status
     * @return CalculationsDTO
     */
    @Override
    public CalculationsDTO calculateTotalAndPercentage(Status status){
        log.info("Accept Status and return calculation and percentage for employee positions.");
        CalculationsDTO calculationsDTO = new CalculationsDTO();
        calculationsDTO.setCountOfEmployees(numberByStatus(status));

        BigDecimal percentage;
        final int constant = 100;
        /**
         * Check if count is Zero
         */
        if(calculationsDTO.getCountOfEmployees() == 0 || employeePositionRepository.count() == 0) {
            percentage = BigDecimal.ZERO;
        } else {
            percentage = new BigDecimal(calculationsDTO.getCountOfEmployees() * constant)
                    .divide(BigDecimal.valueOf(employeePositionRepository.count()), 2, RoundingMode.HALF_UP);
        }
        calculationsDTO.setPercentageOfTotal(percentage);
        return calculationsDTO;
    }
    /**
     * Accept Status and return list of Distributions
     * @param status receives Status
     * @return list of DistributionDTO
     */
    @Override
    public List<DistributionDTO> calculateCountByDiscipline(Status status){
        log.info("Accept Status and return list of Distributions.");
        List<Discipline> disciplines = Arrays.asList(Discipline.JAVA, Discipline.dotNET, Discipline.QA);
        /**
         * Map Disciplines to DistributionDTO
         */
        return disciplines.stream().map(i ->
            new DistributionDTO(i, numberByStatusAndDiscipline(status, i)))
                .collect(Collectors.toList());
    }
    /**
     * Return all employee positions
     * @return list of EmployeePosition
     */
    @Override
    public List<EmployeePosition> findAll() {
        log.info("Return all employee positions.");
        return this.employeePositionRepository.findAll();
    }
    @Override
    public Page<ProjectAssigneesDTO> filterEmployeesByStatus(Status status, Pageable pageable) {

        Page<EmployeePosition> employeeList = employeePositionRepository.findByStatus(status, pageable);

        return employeeList.map(employee -> {
            ProjectAssigneesDTO projectAssigneesDTO = modelMapper.map(employee, ProjectAssigneesDTO.class);
            projectAssigneesDTO.setId(employee.getEmployee().getId());
            projectAssigneesDTO.setFirstName(employee.getEmployee().getFirstName());
            projectAssigneesDTO.setLastName(employee.getEmployee().getLastName());
            return projectAssigneesDTO;
        });
    }
}
