package com.endava.wfp.controllers;

import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.dto.SkillDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.enums.MailType;
import com.endava.wfp.enums.Status;
import com.endava.wfp.service.mail.MailService;
import com.endava.wfp.service.project.ProjectService;
import com.endava.wfp.service.skill.SkillService;
import com.endava.wfp.service.user.EmployeeService;
import com.endava.wfp.utility.Constants;
import com.endava.wfp.utility.exception.StartAndEndDateException;
import com.endava.wfp.utility.exception.UsedEmailException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class AddController {

    private final EmployeeService employeeService;
    private final ProjectService projectService;
    private final SkillService skillService;
    private final MailService mailService;

    private static final String SKILLS = "skills";
    private static final String MANAGERS = "managers";

    /**
     * Going to add page
     * @param model the model where we pass
     *              the information
     *              to the front end
     * @return model and view towards the front end
     */
    @GetMapping("/employee/add/{page}/{sortingProperty}")
    public ModelAndView addEmployee(Model model,
                                    @PathVariable("page") Long page,
                                    @PathVariable("sortingProperty") String sortingProperty) {
        log.info("Open add page");
        List<ProjectNameDTO> projects = this.projectService.findAll();
        model.addAttribute(Constants.PROJECTS, projects);

        List<Employee> managers = this.employeeService.getAllManagers();
        model.addAttribute(MANAGERS, managers);

        List<SkillDTO> skills = this.skillService.findAll();
        model.addAttribute(SKILLS, skills);

        model.addAttribute(Constants.PAGE, page);

        model.addAttribute(Constants.SORTING, sortingProperty);

        if(!model.containsAttribute("dto")) {
            model.addAttribute("dto", new EmployeeAddDTO());
        }

        return new ModelAndView("list/add");
    }

    /**
     * Adding user
     * @param dto data access object
     *            used when an employee
     *            is added
     *
     * @param result binding result
     *               where the message
     *               validation
     *               is passed
     *
     * @param redirectAttr the attributes
     *                     where validation is not passed
     *                     are send here
     *
     * @return model and view
     *         towards the front end
     */
        @PostMapping(value = "/employee/add/{page}/{sortingProperty}")
        public ModelAndView addEmployee(@Valid @ModelAttribute("dto") EmployeeAddDTO dto, BindingResult result,
                @PathVariable("page") Long page,
                @PathVariable("sortingProperty") String sortingProperty,
                RedirectAttributes redirectAttr) throws MessagingException  {
        if (result.hasErrors()) {
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult.dto", result);
            redirectAttr.addFlashAttribute("dto", dto);
            return new ModelAndView("redirect:/employee/add/" + page + "/" + sortingProperty);
        }

        try {
            this.employeeService.add(dto);
        } catch (UsedEmailException err) {
            redirectAttr.addFlashAttribute("usedEmail", err.getMessage());
            redirectAttr.addFlashAttribute("dto", dto);
            return new ModelAndView("redirect:/employee/add/" + page + "/" + sortingProperty);
        } catch (StartAndEndDateException ex) {
            redirectAttr.addFlashAttribute("date", ex.getMessage());
            redirectAttr.addFlashAttribute("dto", dto);
            return new ModelAndView("redirect:/employee/add/" + page + "/" + sortingProperty);
        }

        this.mailService.sendMailMessage(MailType.EMPLOYEE_ADDED, dto.getEmail(), dto);

        log.info("Add user {}", dto);
        redirectAttr.addFlashAttribute("firstName", dto.getFirstName());
        redirectAttr.addFlashAttribute("lastName", dto.getLastName());
        return new ModelAndView("redirect:/employee/add/" + page + "/" + sortingProperty);
    }
    /**
     * Getting all grades
     * @return all grades
     */
    @ModelAttribute(Constants.GRADES)
    public List<String> allGrades() {
        return EnumSet
                .allOf(Grade.class)
                .stream()
                .map(Grade::getValue)
                .collect(Collectors.toList());
    }
    /**
     * Getting all disciplines
     * @return all disciplines
     */
    @ModelAttribute(Constants.DISCIPLINES)
    public List<String> allDisciplines() {
        return EnumSet
                .allOf(Discipline.class)
                .stream()
                .map(Discipline::getValue)
                .collect(Collectors.toList());
    }
    /**
     * Getting all Statuses
     * @return all Statuses
     */
    @ModelAttribute(Constants.STATUSES)
    public List<String> allStatuses() {
        return EnumSet
                .allOf(Status.class)
                .stream()
                .map(Status::getValue)
                .collect(Collectors.toList());
    }
}
