package com.endava.wfp.controllers;

import com.endava.wfp.dto.RequestDTO;
import com.endava.wfp.service.request.RequestAccessService;
import com.endava.wfp.utility.exception.UsedEmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class RequestAccessController {

    @Autowired
    private RequestAccessService requestAccessService;

    /**
     * The method renders
     * the request access form
     *
     * @param model the model where
     *              attributes are stored
     *
     * @return sends the user to
     *         request access screen
     */
    @GetMapping("/requestaccess")
    public ModelAndView showForm(Model model) {
        if(!model.containsAttribute("request")) {
            model.addAttribute("request", new RequestDTO());
        }
        return new ModelAndView("users/requestaccess");
    }
    /**
     * This method handles data
     * coming from the request access form
     * and returns ModelAndView
     * according to it
     *
     * @param request object to which post
     *                form fields are mapped
     *
     * @param bindingResult holds result after
     *                      request object validation
     *
     * @param redirectAttr  contains the context
     *                      for the view after redirect
     *
     * @return ModelAndView the view
     *         and its context
     *         to be rendered
     */
    @PostMapping("/requestaccess")
    public ModelAndView formInfo(@Valid @ModelAttribute("request") RequestDTO request, BindingResult bindingResult, RedirectAttributes redirectAttr) {
        if(bindingResult.hasErrors()) {
            redirectAttr.addFlashAttribute("request", request);
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult.request", bindingResult);
            return new ModelAndView("redirect:/requestaccess");
        }
        try{
            requestAccessService.addRequest(request);
        } catch (UsedEmailException err) {
            redirectAttr.addFlashAttribute("usedEmail", err.getMessage());
            redirectAttr.addFlashAttribute("request", request);
            return new ModelAndView("redirect:/requestaccess");
        }

        redirectAttr.addFlashAttribute("success", Boolean.TRUE);
        return new ModelAndView("redirect:/requestaccess");
    }
}
