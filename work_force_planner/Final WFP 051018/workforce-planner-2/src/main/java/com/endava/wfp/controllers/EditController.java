package com.endava.wfp.controllers;

import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.dto.PageRedirectDTO;
import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.dto.SkillDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.EmployeePosition;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.enums.MailType;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.service.mail.MailService;
import com.endava.wfp.service.project.ProjectService;
import com.endava.wfp.service.skill.SkillService;
import com.endava.wfp.service.user.EmployeeService;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EditController {

    @Autowired
    private EmployeeRepository employeeRepository;

    private final EmployeeService employeeService;
    private final ProjectService projectService;
    private final MailService mailService;
    private static final String EMPLOYEE_VALID = "employeeValid";
    private final SkillService skillService;

    /**
     * Going to edit page
     *
     * @param model model used to store
     *              the information being
     *              send to the front end
     *
     * @param page the current page
     *
     * @param id id of the employee
     *
     * @param sortingProperty the property by
     *                        which the list is sorted
     *
     * @return redirects to the edit page
     */
    @GetMapping("/edit/{page}/{id}/{sortingProperty}")
    public ModelAndView edit(Model model,
                             @PathVariable("page") Long page,
                             @PathVariable("id") Long id,
                             @PathVariable("sortingProperty") String sortingProperty) {
        String positionString = "position";
        String skillsString = "skills";
        /**
         * Find Employee by id
         */
        Employee employee = this.employeeService.findById(id);
        model.addAttribute(Constants.EMPLOYEE, employee);
        /**
         * Return list of Projects from DB
         */
        List<ProjectNameDTO> projects = this.projectService.findAll();
        model.addAttribute(Constants.PROJECTS, projects);
        /**
         * Return list of Skills from DB
         */
        List<SkillDTO> skills = this.skillService.findAll();
        model.addAttribute(skillsString, skills);
        /**
         * Add Page
         */
        model.addAttribute(Constants.PAGE, page);
        /**
         * Add Positions
         */
        model.addAttribute(Constants.SORTING, sortingProperty);

        EmployeePosition position = employee.getEmployeesPositions();
        model.addAttribute(positionString, position);

        if (!model.containsAttribute(EMPLOYEE_VALID)) {
            model.addAttribute(EMPLOYEE_VALID, new EmployeeEditDTO());
        }
        log.info("Editing employee {}", employee);
        return new ModelAndView("users/edit");
    }
    /**
     *
     * @param employee employee dto containing
     *                 all the information the
     *                 user has entered
     *
     * @param result bindingResult where the
     *               messages for wrong information
     *               is stored
     *
     * @param model model the information
     *              send to the front end
     *              is stored
     *
     * @param page the current page
     *             in list of employees
     *
     * @param id the id of the employee
     *
     * @param sortingProperty the property by which
     *                        the list has been sorted
     *
     * @param redirectAttr the attributes
     *                     where validation is not passed
     *                     are send here
     *
     * @return redirects
     *
     */
    @PostMapping("/edit/{page}/{id}/{sortingProperty}")
    public ModelAndView update(@Valid @ModelAttribute(Constants.EMPLOYEE) EmployeeEditDTO employee,
                               BindingResult result,
                               Model model,
                               @PathVariable("page") Long page,
                               @PathVariable("id") Long id,
                               @PathVariable("sortingProperty") String sortingProperty,
                               RedirectAttributes redirectAttr) throws MessagingException {

        if (result.hasErrors()) {
            log.error("Invalid provided data");
            redirectAttr.addFlashAttribute("org.springframework.validation.BindingResult." + EMPLOYEE_VALID, result);
            redirectAttr.addFlashAttribute(EMPLOYEE_VALID, employee);
            return new ModelAndView("redirect:/edit/" + page + "/" + id + "/" + sortingProperty);
        }
        try {
            model.addAttribute("employeeMail", employee.getEmail());
            model.addAttribute(Constants.PAGE, page);
            model.addAttribute(Constants.SORTING, sortingProperty);
            boolean mailHasSent = this.mailService.sendMailMessage(MailType.PROJECT_ASSIGNED, employee.getEmail(), employee);
            log.info("Edit employee {}", employee);
            this.employeeService.edit(employee);

            return mailSent(page, sortingProperty, mailHasSent);
        } catch (IllegalAccessException e) {
            return new ModelAndView("redirect:/edit/" + id + "?error=true");
        }
    }
    /**
     *  Returns a success page if
     *  the employee is assigned
     *  to a new project and mail
     *  has been sent, and redirects
     *  back to list of employees if not
     *
     * @param page the current page
     *             in list of employees
     *
     * @param sortingProperty the property by which
     *                        the list has been sorted
     *
     * @param mailHasSent boolean operator
     *                    showing if an email
     *                    has been send successfully
     *
     * @return redirects
     */
    private ModelAndView mailSent(@PathVariable("page") Long page, @PathVariable("sortingProperty") String sortingProperty, boolean mailHasSent) {
        if (mailHasSent) {
            log.info("Sending email to edited employee");
            return new ModelAndView("mail/text/mailsent");
        } else {
            return new ModelAndView("redirect:/list/" + page + "/" + sortingProperty);
        }
    }
    /**
     * Going back to list of employees
     *
     *
     * @param model model the information
     *              send to the
     *              front end
     *              is stored
     *
     * @param redirect dto where id of employee
     *                 current page in list of employees
     *                 and sorting property is stored
     *
     * @return redirects to edit page
     */
    @PostMapping(value = "/mailsent")
    public ModelAndView back(Model model,
                             PageRedirectDTO redirect) {

        model.addAttribute(Constants.PAGE, redirect.getPage());
        model.addAttribute(Constants.SORTING, redirect.getSortingProperty());

        return new ModelAndView("redirect:/edit/" + redirect.getPage() + "/" + redirect.getSortingProperty());
    }
    /**
     * Getting all grades
     *
     * @return list of all grades
     */
    @ModelAttribute(Constants.GRADES)
    public List<String> allGrades() {
        return EnumSet
                .allOf(Grade.class)
                .stream()
                .map(Grade::getValue)
                .collect(Collectors.toList());
    }
    /**
     * Getting all disciplines
     *
     * @return list of all disciplines
     */
    @ModelAttribute(Constants.DISCIPLINES)
    public List<String> allDisciplines() {
        return EnumSet
                .allOf(Discipline.class)
                .stream()
                .map(Discipline::getValue)
                .collect(Collectors.toList());
    }
    /**
     * Getting all statuses
     *
     * @return list of all statuses
     */
    @ModelAttribute(Constants.STATUSES)
    public List<String> allStatuses() {
        return EnumSet
                .allOf(Status.class)
                .stream()
                .map(Status::getValue)
                .collect(Collectors.toList());
    }
}