package com.endava.wfp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO used for
 * user login
 * information
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeLoginDTO {

    private String email;

    private String password;
}
