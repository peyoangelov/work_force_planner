package com.endava.wfp.utility.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = StartEndDateValidator.class)
@Documented
@Target(value= ElementType.TYPE)
@Retention(value=RUNTIME)
public @interface StartEndDateConstraint {
    String message() default "Start date must be before end date!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
