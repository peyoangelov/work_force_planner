package com.endava.wfp.enums;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum Status {
    BOOKED("Booked"),
    BOOKED_IN_ONE_MONTH("Booked in one month"),
    CURRENT_BENCH("Current bench"),
    BENCH_IN_ONE_MONTH("Bench in one month"),
    BENCH_IN_TWO_MONTHS("Bench in two months");


    private static final Map<String, Status> statusMap;

    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    static {
        Map<String, Status> map = new ConcurrentHashMap<>();

        for (Status s : Status.values()) {
            map.put(s.getValue(), s);
        }
        statusMap = Collections.unmodifiableMap(map);
    }

    public static Status fromValue(String value) {
        Status result = statusMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No constant with text " + value + " found");
        }
        return result;
    }

}