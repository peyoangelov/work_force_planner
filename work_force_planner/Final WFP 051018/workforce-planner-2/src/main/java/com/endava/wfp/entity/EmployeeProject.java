package com.endava.wfp.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity corresponding to
 * table employees_projects with
 * its relationships in the
 * database
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"employee", "project"})
@EqualsAndHashCode(exclude = {"employee", "project"})
@Entity
@Table(name = "employees_projects")
public class EmployeeProject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**Project's start date*/
    @Column(name = "start_date")
    private LocalDate startDate;

    /**Project's end date*/
    @Column(name = "end_date")
    private LocalDate endDate;

    //For Employees relation
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
    //For Employees relation

    //For Projects relation
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
    //For Projects relation

}