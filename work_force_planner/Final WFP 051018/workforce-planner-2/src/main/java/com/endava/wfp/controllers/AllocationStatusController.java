package com.endava.wfp.controllers;


import com.endava.wfp.dto.CalculationsDTO;
import com.endava.wfp.dto.DistributionDTO;
import com.endava.wfp.enums.Status;
import com.endava.wfp.service.position.EmployeePositionService;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class AllocationStatusController {

    @Autowired
    private EmployeePositionService employeeService;

    /**
     * Render the view
     * @return model and view
     *         to the front end
     */
    @GetMapping("/allocationstatus")
    public ModelAndView getAllocationStatusPage() {
        log.info("Render the view of allocation status.");
        return new ModelAndView("charts/allocationstatus");
    }
    /**
     * Get data per status
     * for the total/percent
     * and use it for ajax.
     * The data for the total and %.
     *
     * @param status the status
     *               of the employee
     * @return calculations dto
     */
    @ResponseBody
    @GetMapping("/getAllocationDataForTotal")
    public CalculationsDTO calculateTotalAndPercentage(@RequestParam("status") String status) {
        log.info("Get data per status for the total/percent and use it for ajax.");
        return employeeService.calculateTotalAndPercentage(Status.fromValue(status));
    }
    /**
     * Get data per status
     * for the chart and
     * use it for ajax.
     * The data for the chart.
     *
     * @param status the status
     *               of the employee
     *
     * @return list of dtos where the
     *         discipline and count of
     *         employees is stored
     */
    @ResponseBody
    @GetMapping("/getAllocationDataForChart")
    public List<DistributionDTO> getAllocationDataByStatus(@RequestParam("status") String status) {
        log.info("Get data per status for the chart and use it for ajax.");
        return employeeService.calculateCountByDiscipline(Status.fromValue(status));
    }
    /**
     * Getting all statuses
     * @return returning list
     * of all statuses
     */
    @ModelAttribute(Constants.STATUSES)
    public List<String> allStatuses() {
        log.info("Return all statuses.");
        return EnumSet
                .allOf(Status.class)
                .stream()
                .map(Status::getValue)
                .collect(Collectors.toList());
    }
}
