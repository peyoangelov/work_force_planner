package com.endava.wfp.utility;

/**
 * Class containing all constants
 * used more than once
 * in more than one
 * different classes
 * and scopes!
 */
public final class Constants {

    public static final String GRADES = "grades";
    public static final String DISCIPLINES = "disciplines";
    public static final String STATUSES = "statuses";
    public static final String EMPLOYEE = "employee";
    public static final String PROJECTS = "projects";
    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_REJECTED = "REJECTED";

    public static final int EMPLOYEE_PER_PAGE = 10;

    public static final String SORTING = "sortingProperty";
    public static final String PAGE = "page";

    public static final int buttonsToShow = 10;

    public static final String E_MAIL_SIZE_MESSAGE = "E-mail must be between 1 and 100 characters";
    public static final String FIRST_NAME_SIZE_MESSAGE = "First Name must be between 1 and 100 characters";
    public static final String LAST_NAME_SIZE_MESSAGE = "Last Name must be between 1 and 100 characters";
    public static final String BLANK_FIELD = "Field cannot be blank";
    public static  final String MAIL_SUBJECT = "Changes to your Workforce Planner account have been done";
    public static final String REASON_FOR_REQUEST_MESSAGE = "Size must be between 1 and 300 characters!";

    //search page validation constants
    public static final int FIRST_PAGE = 1;
    public static final int ZERO_PAGE = 0;
    //search page validation constants


    public static final Long TEST_ID = 1l;
}
