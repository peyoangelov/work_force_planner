package com.endava.wfp.service.employee_project;

import com.endava.wfp.dto.ProjectEmployeeDTO;
import com.endava.wfp.entity.EmployeeProject;
import com.endava.wfp.repository.EmployeeProjectRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class EmployeeProjectServiceImpl implements EmployeeProjectService {

    @Autowired
    private EmployeeProjectRepository employeeProjectRepository;
    @Autowired
    private ModelMapper modelMapper;

    /**
     *
     * @param employeeId id of the employee
     *                   we want to
     *                   remove from project
     * @param projectId id of the selected project
     * Removes the employee
     * from the current
     * project
     */
    @Transactional
    @Override
    public void delete(Long employeeId, Long projectId) {

        this.employeeProjectRepository.deleteByEmployeeIdAndProjectId(employeeId,projectId);
    }
    /**
     * @param projectId the id of
     *                  the selected project
     * @param pageable variable for pagination
     * @return List of ProjectEmployeeDTOs
     * Creates a DTO for
     * every entry
     * in database
     * with the
     * given projectID
     */
    @Override
    public Page<ProjectEmployeeDTO> findAllByProject(Long projectId, Pageable pageable) {

        Page<EmployeeProject> employeeProjectPage = this.employeeProjectRepository.findAllByProjectId(projectId, pageable);

        return employeeProjectPage.map(e -> {
            ProjectEmployeeDTO projectEmployeeDTO = modelMapper.map(e, ProjectEmployeeDTO.class);

            projectEmployeeDTO.setId(e.getEmployee().getId());
            projectEmployeeDTO.setName(e.getEmployee().getFirstName() + " " + e.getEmployee().getLastName());
            projectEmployeeDTO.setDiscipline(e.getEmployee().getEmployeesPositions().getDiscipline().getValue());
            projectEmployeeDTO.setGrade(e.getEmployee().getEmployeesPositions().getGrade().getValue());

            return projectEmployeeDTO;
        });
    }
}
