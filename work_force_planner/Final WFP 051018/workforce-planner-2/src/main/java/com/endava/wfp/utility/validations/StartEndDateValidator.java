package com.endava.wfp.utility.validations;

import com.endava.wfp.dto.interfaces.IStartEndDateForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class StartEndDateValidator implements ConstraintValidator<StartEndDateConstraint, Object> {
    /**
     * Check if start Data and End Date are valid
     * @param object The object received
     *               from the place where
     *               the annotation was added
     * @param constraintValidatorContext this is the anntoation
     *                                   interface
     * @return  true or false
     *          returns if the
     *          following class fields
     *          are valid
     */
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        IStartEndDateForm startEndDateForm = (IStartEndDateForm) object;
        LocalDate startDate = startEndDateForm.getStartDate();
        LocalDate endDate = startEndDateForm.getEndDate();
        /*
         If either one is empty runtime continues until it reaches @NotNUll
        */
        if(startDate == null || endDate == null){
            return true;
        }
        return dateIsBefore(startDate,endDate);
    }
    /**
     * Check is start date is before end date
     * @param startDate the start date
     *                  of a class
     * @param endDate the end date
     *                of a class
     * @return  true if the
     *          startDate is before endDate
     *          or
     *          false if the
     *          endDate is before startDate
     */
    private boolean dateIsBefore(LocalDate startDate,LocalDate endDate){
        return startDate.isBefore(endDate);
    }
}
