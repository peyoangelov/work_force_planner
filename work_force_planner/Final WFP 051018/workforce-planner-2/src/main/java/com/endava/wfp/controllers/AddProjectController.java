package com.endava.wfp.controllers;

import com.endava.wfp.dto.ProjectAddDTO;
import com.endava.wfp.service.project.ProjectService;
import com.endava.wfp.utility.exception.ExistingProjectException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AddProjectController {

    private final ProjectService projectService;
    private String projectValid = "projectValid";

    /**
     * Display page for Projects
     * @param model
     * @return ModelAndView for page for Projects
     */
    @GetMapping("/project/add")
    public ModelAndView add(Model model) {

        if (!model.containsAttribute(projectValid)) {
            model.addAttribute(projectValid, new ProjectAddDTO());
        }
        return new ModelAndView("project/addProject.html");
    }

    /**
     * Add new Project
     * @param project receives ProjectAddDTO
     * @param result validation result for request
     * @param redirectAttributes RedirectAttributes with parameters for the thymeleaf
     * @return ModelAndView for redirect
     */
    @PostMapping("/project/add")
    public ModelAndView addProject(@Valid @ModelAttribute("project") ProjectAddDTO project,
                                   BindingResult result,
                                   RedirectAttributes redirectAttributes) {

        final String projectAddUrl = "redirect:/project/add";
        final String error = "existingProject";
        final String projectName = "projectName";
        /**
         * Start and End date validation
         */
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult." + projectValid, result);
            redirectAttributes.addFlashAttribute(projectValid, project);
            return new ModelAndView(projectAddUrl);
        }
        /**
         * Adds the project if it does not already exist
         */
        try {
            this.projectService.add(project);
        } catch (ExistingProjectException e) {
            redirectAttributes.addFlashAttribute(error, e.getMessage());
            return new ModelAndView(projectAddUrl);
        }

        redirectAttributes.addFlashAttribute(projectName, project.getProjectName());
        return new ModelAndView(projectAddUrl);
    }
}
