package com.endava.wfp.service.pagination;
import com.endava.wfp.dto.EmployeePageDTO;
import com.endava.wfp.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.servlet.ModelAndView;

/**
 * Interface for MEmployeePaginationImpl class for override methods
 */
public interface EmployeePagination{
    Page<EmployeePageDTO> findAll(Pageable pageable);
    ModelAndView redirectSearchPage(Page<?> pageList, Integer selectedPage, String redirectionPage);
    ModelAndView redirectSearchPageByFilteringProperty(Page<?> pageList, Integer projectsPage , Integer selectedPage, Status filteringProperty, String projectName);
    ModelAndView redirectSearchPageBySortingProperty(Page<?> pageList,
                                                     Integer selectedPage,
                                                     String sortingProperty,
                                                     String redirectionPage);
    ModelAndView redirectSearchPageByProject(Page<?> pageList, Integer selectedPage, String redirectionPage, String project);
    Integer selectedPageInBounds(Page<?>pageList, Integer selectedPage);
}