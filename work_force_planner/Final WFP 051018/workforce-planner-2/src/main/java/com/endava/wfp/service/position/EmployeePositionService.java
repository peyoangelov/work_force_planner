package com.endava.wfp.service.position;

import com.endava.wfp.dto.CalculationsDTO;
import com.endava.wfp.dto.DistributionDTO;
import com.endava.wfp.dto.ProjectAssigneesDTO;
import com.endava.wfp.entity.EmployeePosition;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
/**
 * Interface for MEmployeePaginationImpl class for override methods
 */
public interface EmployeePositionService {
    Long numberByStatus(Status status);
    Long numberByStatusAndDiscipline(Status status, Discipline discipline);
    CalculationsDTO calculateTotalAndPercentage(Status status);
    List<DistributionDTO> calculateCountByDiscipline(Status status);
    List<EmployeePosition> findAll();
    Page<ProjectAssigneesDTO> filterEmployeesByStatus(Status status, Pageable pageable);
}
