package com.endava.wfp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Slf4j
public class LoginController {

    /**
     * Visualising the login page
     *
     * @return sends the user
     *         to login page
     */
    @GetMapping("/login")
    public ModelAndView login() {
        //Logging when the user goes to login page
        log.info("Login page");
        return new ModelAndView("users/login");
    }
}