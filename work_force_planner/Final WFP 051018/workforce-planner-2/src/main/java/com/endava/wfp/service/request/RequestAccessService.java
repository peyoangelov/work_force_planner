package com.endava.wfp.service.request;

import com.endava.wfp.dto.RequestDTO;
import com.endava.wfp.dto.ReviewRequestAccessDto;
import com.endava.wfp.entity.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
/**
 * Interface for RequestAccessServiceImpl class for override methods
 */
public interface RequestAccessService {
    List<RequestDTO> findByStatus(String status);
    Request requestStatus(String status, Long id);
    void addRequest(RequestDTO request);
    Page<ReviewRequestAccessDto> findAllRequestsByPage(Pageable pageable);

}
