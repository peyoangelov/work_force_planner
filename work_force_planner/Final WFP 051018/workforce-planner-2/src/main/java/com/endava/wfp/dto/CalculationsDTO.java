package com.endava.wfp.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Dto used for calculating
 * the count of employees on a
 * current status and the percentage
 * of employees on a given status
 */
@Data
public class CalculationsDTO {

    // the count of employees with a given status
    private Long countOfEmployees;

    // the percentage of employees with a given status from all employees
    private BigDecimal percentageOfTotal;
}
