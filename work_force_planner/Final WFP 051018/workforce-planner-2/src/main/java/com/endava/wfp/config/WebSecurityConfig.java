package com.endava.wfp.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    @Autowired
    public WebSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    /**
     * Allowing anonymous access on /login
     * so that users can authenticate.
     * The user should enter valid password and e-mail
     * and if it is a successful login -
     * user is redirected to the dashboard page.
     * Everyone is authorized to access RequestAccess page.
     * loginPage() – the custom login page
     * defaultSuccessUrl() – the landing page after a successful login
     * failureUrl() – the landing page after an unsuccessful login
     * logout() - the custom logout page
     * @param http http security
     * @throws Exception
     */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().formLogin()
                .loginPage("/login").permitAll()
                .passwordParameter("password")
                .usernameParameter("email")
                .failureUrl("/login?error=true")
                .defaultSuccessUrl("/dashboard", true)
                .and().authorizeRequests().antMatchers("/requestaccess").permitAll().and().authorizeRequests()
                .antMatchers("/**").authenticated()
                .and()
                .logout()
                .permitAll()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .and()
                .userDetailsService(this.userDetailsService);
    }
}
