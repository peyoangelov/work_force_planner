package com.endava.wfp.service.project;

import com.endava.wfp.dto.ProjectAddDTO;
import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
/**
 * Interface for ProjectServiceImpl class for override methods
 */
public interface ProjectService {
    List<ProjectNameDTO> findAll();
    void addEmployeesToProject(ArrayList<Long> ids, ArrayList<String> startDates, ArrayList<String> endDates, String projectName);
    void changeStatus(Employee employee, LocalDate startDate, LocalDate endDate);
    Boolean validateDates(LocalDate startDate, LocalDate endDate, String projectName);
    Page<ProjectNameDTO> findAllProjectByPage(Pageable pageable);
    Project add(ProjectAddDTO model);
}