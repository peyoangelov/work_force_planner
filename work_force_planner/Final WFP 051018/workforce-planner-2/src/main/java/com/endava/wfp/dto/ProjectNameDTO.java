package com.endava.wfp.dto;

import com.endava.wfp.utility.Constants;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * DTO used to add Project
 * and get its ID
 */
@Data
public class ProjectNameDTO {
    private Long id;

    @NotBlank(message = Constants.BLANK_FIELD)
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @NotBlank(message = Constants.BLANK_FIELD)
    private String description;
}
