package com.endava.wfp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *  This class contains info needed for sending e-mails
 */
@Data
@ConfigurationProperties(prefix="spring.mail")
public class MailProperties {

    private String sender;
    private String defaultEncoding;
    private final Server server = new Server();

    /**
     * This class contains information about the Server from witch the e-mails are being send
     */
    @Data
    public static class Server {

        private String host;
        private int port;
        private String protocol;
        private String username;
        private String password;
    }
}
