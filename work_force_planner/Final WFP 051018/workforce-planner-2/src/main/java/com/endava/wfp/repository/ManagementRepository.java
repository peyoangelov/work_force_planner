package com.endava.wfp.repository;

import com.endava.wfp.entity.EmployeeHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagementRepository extends JpaRepository<EmployeeHistory, Long> {
}
