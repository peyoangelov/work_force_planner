package com.endava.wfp.dto;

import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for Add Employees
 * button on View Project Page
 * to show filtered employees
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjectAssigneesDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private Discipline discipline;

    private Grade grade;

    private Status status;
}
