package com.endava.wfp.service.request;

import com.endava.wfp.dto.ProjectAssigneesDTO;
import com.endava.wfp.dto.RequestDTO;
import com.endava.wfp.dto.ReviewRequestAccessDto;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.Request;
import com.endava.wfp.enums.RequestStatus;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.repository.RequestRepository;
import com.endava.wfp.service.user.EmployeeServiceImpl;
import com.endava.wfp.utility.Constants;
import com.endava.wfp.utility.exception.UsedEmailException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RequestAccessServiceImpl implements RequestAccessService {

    private final RequestRepository requestRepository;
    private final EmployeeRepository employeeRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    /**
     * Accept Status and return RequestStatus enum
     * @param status receives Status
     * @return RequestStatus with set value
     */
    private RequestStatus convertStringToRequestStatus(String status) {
        log.info("Return RequestStatus enum.");
        return RequestStatus.valueOf(status.toUpperCase());
    }
    /**
     * Accept Status and return list of requests
     * @param status receives Status
     * @return list of RequestDTO
     */
    @Override
    public List<RequestDTO> findByStatus(String status) {
        log.info("Return list of requests");
        /**
         * Map Request to RequestDTO
         */
        return this.requestRepository.findByStatus(convertStringToRequestStatus(status))
                .stream()
                .map(request -> modelMapper.map(request, RequestDTO.class))
                .collect(Collectors.toList());
    }
    /**
     * Accept Status, id, return Request with changed status
     * and save it in DB
     * @param status receives Status
     * @param id receives id
     * @return Request
     */
    @Override
    @Transactional
    public Request requestStatus(String status, Long id) {
        log.info("return Request, change status and saved it to db");
        /**
         * Find Request by id
         */
        Request request = this.requestRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("No such request found"));

        /**
         * Get the current user email
         */
        String currentUserEmail = EmployeeServiceImpl.currentUserDetails().getUsername();

        /**
         * Get
         * the manager's first name and last name
         * who has revised
         * the request
         */
        String managerFirstName = this.employeeRepository.findFirstByEmail(currentUserEmail).get().getFirstName();
        String managerLastName = this.employeeRepository.findFirstByEmail(currentUserEmail).get().getLastName();

        request.setStatus(convertStringToRequestStatus(status));

        /**
         * Set
         * the manager's full name
         */
        request.setRevisedManagerFullName(managerFirstName + " " + managerLastName);

        /**
         * Set
         * the revised request's date and time
         */
        request.setRevisedDateTime(LocalDateTime.now());

        /**
         * Set
         * the manager's email
         * who has revised
         * the request
         */
        request.setRevisedManagerEmail(currentUserEmail);
        /**
         * Check Status and set Employee
         */
        switch (status) {
            case Constants.STATUS_APPROVED:
                Employee employee = this.modelMapper.map(request, Employee.class);
                employee.setId(null);
                employee.setPassword(this.passwordEncoder.encode("password"));

                Employee currentUser = this.employeeRepository.findFirstByEmail(currentUserEmail)
                        .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
                employee.setManager(currentUser);
                this.employeeRepository.save(employee);
                request.setStatus(RequestStatus.valueOf(Constants.STATUS_APPROVED));
                break;
            case Constants.STATUS_REJECTED:
                request.setStatus(RequestStatus.valueOf(Constants.STATUS_REJECTED));
                break;
            default:
                break;
        }
        return this.requestRepository.save(request);
    }
    /**
     * This method persists new Request entity based on the RequestDTO object
     * which it takes as an argument
     * @param requestDTO holds info about the request to be added
     */
    @Override
    public void addRequest(RequestDTO requestDTO) {
        /**
         * Check if email exist
         */
        if(employeeRepository.existsByEmail(requestDTO.getEmail())) {
            throw new UsedEmailException("User with the same e-mail already exists");
        }
        /**
         * Check if email exist
         */
        if(requestRepository.existsByEmail(requestDTO.getEmail())) {
            throw new UsedEmailException("Request with this e-mail has already been sent");
        }

        Request request = modelMapper.map(requestDTO, Request.class);
        request.setRequestDate(LocalDateTime.now());
        request.setStatus(RequestStatus.PENDING);
        requestRepository.save(request);
    }

    @Override
    public Page<ReviewRequestAccessDto> findAllRequestsByPage(Pageable pageable) {
        Page<Request> allRequests = this.requestRepository.findAll(pageable);

        return allRequests.map(request -> modelMapper.map(request, ReviewRequestAccessDto.class));
    }
}
