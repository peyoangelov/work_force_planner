package com.endava.wfp.dto;

import com.endava.wfp.enums.MailType;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * DTO used for email
 * send to employees
 */
@Data
public class MailDTO {

    private String recipient;
    private MailType messageType;

    @Autowired
    public MailDTO(MailType messageType, String recipient) {
        this.messageType = messageType;
        this.recipient = recipient;
    }
}
