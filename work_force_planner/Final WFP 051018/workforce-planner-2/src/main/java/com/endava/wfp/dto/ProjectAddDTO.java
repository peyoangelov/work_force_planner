package com.endava.wfp.dto;


import com.endava.wfp.dto.interfaces.IStartEndDateForm;
import com.endava.wfp.utility.Constants;
import com.endava.wfp.utility.validations.StartEndDateConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 *DTO used for saving
 * a project in the database
 * from Add Project View
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@StartEndDateConstraint
public class ProjectAddDTO implements IStartEndDateForm {
    private static final String NOT_BLANK_PROJECT_NAME = "Project name must be between 1 and 100 characters";
    private static final String DESCRIPTION_LIMIT_MESSAGE = "Description must be at most 255 characters";
    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(min = 1, max = 100, message = NOT_BLANK_PROJECT_NAME)
    private String projectName;

    //the project's description
    @NotBlank(message = Constants.BLANK_FIELD)
    @Size(max = 255, message = DESCRIPTION_LIMIT_MESSAGE)
    private String description;

    //the starting date of the project
    @NotNull(message = Constants.BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    //the date for project completion/end
    @NotNull(message = Constants.BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

}
