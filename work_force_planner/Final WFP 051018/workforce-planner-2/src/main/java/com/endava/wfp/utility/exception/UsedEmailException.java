package com.endava.wfp.utility.exception;

/**
 * Exception class for the
 * used email validation
 */
public class UsedEmailException extends RuntimeException {
    public UsedEmailException(String errorMessage) {
        super(errorMessage);
    }
}
