package com.endava.wfp.controllers;

import com.endava.wfp.dto.ReviewRequestAccessDto;
import com.endava.wfp.model.PageCreator;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.service.request.RequestAccessService;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ReviewRequestScreenController {

    private final RequestAccessService requestAccessService;
    private final EmployeePagination employeePagination;

    @GetMapping("/management/revisedRequest/{page}")
    public ModelAndView getRevisedRequestPage(@PathVariable("page") Integer page, @RequestParam(value = "selectedPage", required = false) Integer selectedPage) {

        final String REDIRECT_PAGE_REVISE_REQUESTS = "management/revisedRequest";

        ModelAndView modelAndView = new ModelAndView("management/requestReviewScreen");

        if (page < Constants.FIRST_PAGE) {
            page = Constants.FIRST_PAGE;
        }

        int pageIndex = page - 1;

        Page<ReviewRequestAccessDto> allRevisedRequestsPage = requestAccessService.findAllRequestsByPage(PageRequest.of(pageIndex, Constants.EMPLOYEE_PER_PAGE, Sort.by("id").descending()));

        PageCreator pageCreator = new PageCreator(allRevisedRequestsPage.getTotalPages() , allRevisedRequestsPage.getNumber() + 1);

        modelAndView.addObject("allRevisedRequestsPage", allRevisedRequestsPage);
        modelAndView.addObject("pageCreator", pageCreator);
        modelAndView.addObject("pageNumber", allRevisedRequestsPage.getNumber());
        modelAndView.addObject("maxPages", allRevisedRequestsPage.getTotalPages());

        if (selectedPage != null) {
            modelAndView = employeePagination.redirectSearchPage(allRevisedRequestsPage, selectedPage, REDIRECT_PAGE_REVISE_REQUESTS);
        }
        return modelAndView;
    }
}