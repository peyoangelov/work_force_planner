package com.endava.wfp.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.io.IOException;
import java.util.Collections;

@Data
@Configuration
@EnableConfigurationProperties(MailProperties.class)
public class MailConfig {

    public static final String RESOLVABLE_PATTERNS = "text/*";
    public static final String PREFIX = "/mail/";
    public static final String SUFFIX = ".html";
    public static final int ORDER = 1;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Environment environment;

    @Autowired
    public MailProperties mailProperties;

    @Bean
    public JavaMailSender mailSender() throws IOException {

        final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(this.mailProperties.getServer().getHost());
        mailSender.setPort(this.mailProperties.getServer().getPort());
        mailSender.setProtocol(this.mailProperties.getServer().getProtocol());
        mailSender.setUsername(this.mailProperties.getServer().getUsername());
        mailSender.setPassword(this.mailProperties.getServer().getPassword());
        mailSender.setDefaultEncoding(this.mailProperties.getDefaultEncoding());

        return mailSender;
    }

    @Bean
    public TemplateEngine emailTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(htmlTemplateResolver());
        return templateEngine;
    }

    private ITemplateResolver htmlTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setOrder(ORDER);
        templateResolver.setResolvablePatterns(Collections.singleton(RESOLVABLE_PATTERNS));
        templateResolver.setPrefix(PREFIX);
        templateResolver.setSuffix(SUFFIX);
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding(this.mailProperties.getDefaultEncoding());
        templateResolver.setCacheable(false);
        return templateResolver;
    }
}