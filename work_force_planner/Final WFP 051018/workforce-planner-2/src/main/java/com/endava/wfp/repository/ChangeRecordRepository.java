package com.endava.wfp.repository;

import com.endava.wfp.entity.ChangeRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChangeRecordRepository extends JpaRepository<ChangeRecord, Long> {

}
