package com.endava.wfp.repository;

import com.endava.wfp.entity.EmployeePosition;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * contains methods which
 * were used in Allocation Status
 * and Add Employees in View Project Page
 */
@Repository
public interface EmployeePositionRepository  extends JpaRepository<EmployeePosition, Long> {

    Long countByStatus(Status status);
    Long countByStatusAndDiscipline(Status status, Discipline discipline);
    List<EmployeePosition> findAll();
    Page<EmployeePosition> findByStatus(Status status, Pageable pageble);
}