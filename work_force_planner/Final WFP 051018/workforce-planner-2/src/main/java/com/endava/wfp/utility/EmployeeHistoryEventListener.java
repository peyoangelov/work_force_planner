package com.endava.wfp.utility;

import com.endava.wfp.entity.EmployeeHistory;

import javax.persistence.PostLoad;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class EmployeeHistoryEventListener  {
    /**
     * Set date to UTC
     * @param employeeHistory receives EmployeeHistory
     */
    @PreUpdate
    public void setCurrentDateToUTC(EmployeeHistory employeeHistory) {
        employeeHistory.setModifiedDate(LocalDateTime.now(ZoneOffset.UTC));
    }
    /**
     * Change Date to TimeZone
     * @param employeeHistory
     */
    @PostLoad
    public void changeCurrentDateToRightTimeZone(EmployeeHistory employeeHistory) {
        OffsetDateTime offsetDateTime = employeeHistory.getModifiedDate().atOffset(ZoneOffset.UTC);
        ZoneOffset zoneOffset = OffsetDateTime.now(ZoneId.of("+3")).getOffset();
        employeeHistory.setModifiedDate(offsetDateTime
                .withOffsetSameInstant(zoneOffset)
                .toLocalDateTime());
    }
}
