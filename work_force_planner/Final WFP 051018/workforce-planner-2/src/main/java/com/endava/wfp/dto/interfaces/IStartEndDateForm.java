package com.endava.wfp.dto.interfaces;

import java.time.LocalDate;
/**
 *Interface used for validation
 * of startDate and endDate
 *(startDate must be after endDate)
 */
public interface IStartEndDateForm {

    LocalDate getStartDate();
    LocalDate getEndDate();
}
