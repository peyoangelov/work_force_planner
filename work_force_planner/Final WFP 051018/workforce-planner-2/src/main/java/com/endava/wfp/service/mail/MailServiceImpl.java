package com.endava.wfp.service.mail;

import com.endava.wfp.config.MailProperties;
import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.dto.MailDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.enums.MailType;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class MailServiceImpl implements MailService {

    private final EmployeeRepository employeeRepository;
    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;
    private final MailProperties mailProperties;

    private static final String EMAIL_EMPLOYEEADDED_TEMPLATE_NAME = "mail/text/user-added-template";
    private static final String EMAIL_PROJECTCHANGED_TEMPLATE_NAME = "mail/text/project-assigned-template";
    private static final String EMAIL_EMPLOYEEASSIGNED_TEMPLATE_NAME = "mail/text/employee-assigned-template";

    private static final String EXCEPTIONMESSAGE = "Employee has not been found";

    /**
     * Sending mail to employee
     * @param messageType receive enum MailType with value Employee added or Project assigned
     * @param recipient receive mail of recipient - Employee
     * @param dto receive changes which has been made
     * @return true or false
     */
    @Override
    public boolean sendMailMessage(MailType messageType, String recipient, EmployeeEditDTO dto) {
        /**
         * Find Employee by id in DB
         */
        Employee employee = this.employeeRepository.findById(
                dto.getId())
                .orElseThrow(() -> new UsernameNotFoundException(EXCEPTIONMESSAGE));
        /**
         * Map EmployeeProjects to List of Strings
         */
        List<String> projectNames = employee.getEmployeesProjects()
                .stream()
                .map(e -> e.getProject().getName())
                .collect(Collectors.toList());

        /**
         * Check if projects contains the Project from editing form
         */
        if (!projectNames.contains(dto.getProject())) {
            Context context = new Context();
            context.setVariable("employee", dto);
            sendMail(messageType, recipient, context);
            return true;
        } else {
            return false;
        }
    }
    /**
     * Settings for mail
     * @param messageType receive enum MailType with value Employee added or Project assigned
     * @param recipient receive mail of recipient - Employee
     * @param context receive parameters for thymeleaf in html
     * @return true or false
     */
    boolean sendMail(MailType messageType, String recipient, Context context) {

        MailDTO mailDTO = new MailDTO(messageType, recipient);
        String processedHTMLTemplate = null;
        /**
         * Check value of MailType enum
         */
        if (messageType == MailType.EMPLOYEE_ADDED) {
            processedHTMLTemplate = templateEngine.process(EMAIL_EMPLOYEEADDED_TEMPLATE_NAME, context);
        } else if (messageType == MailType.PROJECT_ASSIGNED) {
            processedHTMLTemplate = templateEngine.process(EMAIL_PROJECTCHANGED_TEMPLATE_NAME, context);
        } else if (messageType == MailType.EMPLOYEE_ASSIGNED) {
            processedHTMLTemplate = templateEngine.process(EMAIL_EMPLOYEEASSIGNED_TEMPLATE_NAME, context);
        }
        String finalProcessedHTMLTemplate = processedHTMLTemplate;
        /**
         * Create and set MimeMessagePreparator
         */
        MimeMessagePreparator preparator = message -> {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED, "UTF-8");
            helper.setFrom(this.mailProperties.getSender());
            helper.setTo(mailDTO.getRecipient());
            helper.setSubject(Constants.MAIL_SUBJECT);
            assert finalProcessedHTMLTemplate != null;
            helper.setText(finalProcessedHTMLTemplate, true);
        };
        try{
            /**
             * Send mail
             */
            javaMailSender.send(preparator);
            log.info("Mail send to employee");
            return true;
        } catch (MailException e) {
            log.error("Sending e-mail failed!",e);
        }
        return false;
    }
    /**
     * Send employee data to html thymeleaf
     * @param messageType receive enum MailType with value Employee added or Project assigned
     * @param recipient receive mail of recipient - Employee
     * @param dto receive changes which has been made
     * @return true or false
     */
    @Override
    public boolean sendMailMessage(MailType messageType, String recipient, EmployeeAddDTO dto) {
        Context context = new Context();
        context.setVariable("employee", dto);
        sendMail(messageType, recipient, context);
        return true;
    }
    /**
     * Sent mail to recipient
     * @param messageType type of message
     * @param recipient receives mail
     * @param employee receives Employee
     * @return true or false
     * @throws MessagingException
     */
    @Override
    public boolean sendMailMessage(MailType messageType, String recipient, Employee employee) throws MessagingException {
        Context context = new Context();
        context.setVariable("employee", employee);
        sendMail(messageType, recipient, context);
        return true;
    }
}

