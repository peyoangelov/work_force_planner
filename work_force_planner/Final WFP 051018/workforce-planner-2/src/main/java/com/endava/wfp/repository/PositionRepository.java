package com.endava.wfp.repository;

import com.endava.wfp.entity.EmployeePosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<EmployeePosition, Long> {
}
