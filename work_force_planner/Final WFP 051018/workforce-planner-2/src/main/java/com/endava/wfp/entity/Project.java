package com.endava.wfp.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

/**
 * Entity corresponding to
 * table projects with
 * its relationships in the
 * database
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "employeesProjects")
@EqualsAndHashCode(exclude = "employeesProjects")
@Entity
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Size(max = 255)
    @Column(name = "name", unique = true)
    private String name;

    @Size(max = 255)
    @Column(name = "description")
    private String description;
    
    //For EmployeesProjects relation
    @OneToMany(mappedBy = "project")
    private List<EmployeeProject> employeesProjects;
    //For EmployeesProjects relation

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

}
