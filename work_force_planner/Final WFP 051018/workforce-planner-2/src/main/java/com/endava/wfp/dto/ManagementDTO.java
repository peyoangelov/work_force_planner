package com.endava.wfp.dto;

import com.endava.wfp.enums.Action;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * DTO used for
 * history of modifications
 * of employee data
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManagementDTO {

    private Long id;

    private Action action;

    private LocalDateTime modifiedDate;

    private String newValue;

    private String oldValue;

    private Long employeeId;

    private String fullName;
}
