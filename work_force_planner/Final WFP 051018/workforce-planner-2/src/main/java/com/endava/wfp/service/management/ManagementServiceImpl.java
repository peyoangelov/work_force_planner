package com.endava.wfp.service.management;

import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.dto.ManagementDTO;
import com.endava.wfp.entity.ChangeRecord;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.EmployeeHistory;
import com.endava.wfp.entity.EmployeeProject;
import com.endava.wfp.enums.Action;
import com.endava.wfp.repository.ManagementRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ManagementServiceImpl implements ManagementService {

    private final ManagementRepository managementRepository;
    private final ModelMapper modelMapper;
    private final ChangeRecordService changeRecordService;

    private static final String STATUS = "Status";
    private static final String PASSWORD = "Password";
    private static final String PROJECT = "Project";
    private static final String DISCIPLINE = "Discipline";
    private static final String GRADE = "Grade";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String PROJECT_START_DATE = "Project start Date";
    private static final String PROJECT_END_DATE = "Project end Date";
    private static final String EMAIL = "e-mail";
    private static final String PASSWORD_HAS_BEEN_CHANGED = "Password has been changed";

    /**
     * Accept pageable and Find all history entries
     * and map them to dto
     * @param pageable receive pageable Interface
     * @return Page with ManagementDTO
     */
    @Override
    public Page<ManagementDTO> findAll(Pageable pageable) {
        /**
         * Accept pageable and return Page with EmployeeHistory from DB
         */
        Page<EmployeeHistory> managementList = this.managementRepository.findAll(pageable);
        log.info("Finding all history entries and mapping them to dto.");
        /**
         * Map EmployeeHistory to ManagementDTO
         * and set Parameters
         */
        return managementList.map(record -> {
            ManagementDTO dto = modelMapper.map(record, ManagementDTO.class);
            dto.setNewValue(record.getChangeRecord()
                    .stream()
                    .filter(Objects::nonNull)
                    .map(ChangeRecord::getNewValue)
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining(", ")));
            
            dto.setOldValue(record.getChangeRecord()
                    .stream()
                    .map(ChangeRecord::getOldValue)
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining(", ")));
            return dto;
        });
    }
    /**
     * Accept Employee and add audit record when deleting a user
     * @param employee receives Employee who has been deleted
     * @return EmployeeHistory for deleted Employee
     */
    @Override
    @Transactional
    public EmployeeHistory deleteEmployeeAudit(Employee employee) {
        EmployeeHistory employeeHistory = new EmployeeHistory();
        List<ChangeRecord> changeRecords = new ArrayList<>();

        employeeHistory.setAction(Action.DELETED);
        employeeHistory.setEmployeeId(employee.getId());
        employeeHistory.setFullName(String.format("%s %s", employee.getFirstName(), employee.getLastName()));
        /**
         * save EmployeeHistory in DB
         */
        EmployeeHistory employeeHistoryRes = this.managementRepository.save(employeeHistory);

        changeRecords.add(new ChangeRecord(FIRST_NAME, employee.getFirstName(), null, employeeHistoryRes));
        changeRecords.add(new ChangeRecord(LAST_NAME, employee.getLastName(), null, employeeHistoryRes));
        changeRecords.add(new ChangeRecord(EMAIL, employee.getEmail(), null, employeeHistoryRes));
        /**
         * check if EmployeesPositions is not null
         */
        if(employee.getEmployeesPositions() != null) {
            changeRecords.add(new ChangeRecord(DISCIPLINE, employee.getEmployeesPositions().getDiscipline().getValue(),
                    null, employeeHistoryRes));
        }
        /**
         * save all ChangeRecord in DB
         */
        List<ChangeRecord> changeRecordRes = this.changeRecordService.saveAll(changeRecords);
        employeeHistoryRes.setChangeRecord(changeRecordRes);
        log.info("Add audit record when deleting a user");
        /**
         * save EmployeeHistory in DB
         */
        return this.managementRepository.save(employeeHistoryRes);
    }
    /**
     * Accept Employee and add audit record when saving a user
     * @param employee receives Employee who has been saved
     * @return EmployeeHistory
     */
    @Override
    @Transactional
    public EmployeeHistory saveHistoryAudit(Employee employee) {
        EmployeeHistory employeeHistory = new EmployeeHistory();

        employeeHistory.setAction(Action.INSERTED);
        employeeHistory.setEmployeeId(employee.getId());
        employeeHistory.setFullName(String.format("%s %s", employee.getFirstName(), employee.getLastName()));
        /**
         * save EmployeeHistory in DB
         */
        EmployeeHistory employeeHistoryRes = this.managementRepository.save(employeeHistory);

        List<ChangeRecord> changeRecords = new ArrayList<>();

        changeRecords.add(new ChangeRecord(FIRST_NAME, null, employee.getFirstName(), employeeHistoryRes));
        changeRecords.add(new ChangeRecord(LAST_NAME, null, employee.getLastName(), employeeHistoryRes));
        changeRecords.add(new ChangeRecord(EMAIL,null, employee.getEmail(), employeeHistoryRes));
        changeRecords.add(new ChangeRecord(DISCIPLINE, null,
                employee.getEmployeesPositions().getDiscipline().getValue(), employeeHistoryRes));

        List<ChangeRecord> changeRecordRes = this.changeRecordService.saveAll(changeRecords);

        employeeHistoryRes.setChangeRecord(changeRecordRes);
        log.info("Add audit record when saving a user");
        /**
         * save EmployeeHistory in DB
         */
        return this.managementRepository.save(employeeHistoryRes);
    }
    /**
     * Accept Employee, EmployeeEditDTO
     * and add audit record when editing a user
     * @param currentUser receives current Employee
     * @param dto receives dto with all changes
     * @return EmployeeHistory
     */
    @Override
    @Transactional
    public EmployeeHistory editEmployeeAudit(Employee currentUser, EmployeeEditDTO dto) {
        EmployeeHistory employeeHistory = new EmployeeHistory();
        employeeHistory.setAction(Action.UPDATED);
        employeeHistory.setEmployeeId(currentUser.getId());
        employeeHistory.setFullName(String.format("%s %s", currentUser.getFirstName(), currentUser.getLastName()));
        /**
         * save EmployeeHistory in DB
         */
        EmployeeHistory employeeHistoryRes = this.managementRepository.save(employeeHistory);

        List<ChangeRecord> changeRecords = new ArrayList<>();
        /**
         * check EmployeesPosition is not null
         * and current Employee Grade is not equals new Grade
         */
        if (currentUser.getEmployeesPositions() != null &&
                !dto.getGrade().equals(currentUser.getEmployeesPositions().getGrade().getValue())) {

            changeRecords.add(new ChangeRecord(GRADE,
                    currentUser.getEmployeesPositions().getGrade().getValue(),dto.getGrade(),
                    employeeHistoryRes));
        } else {
            changeRecords.add(new ChangeRecord(GRADE,
                    null, dto.getGrade() , employeeHistoryRes));
        }
        /**
         * check EmployeesPositions is not null
         * and Employee Discipline in not equals to new Discipline
         */
        if (currentUser.getEmployeesPositions() != null &&
                !dto.getDiscipline().equals(currentUser.getEmployeesPositions().getDiscipline().getValue())) {
            changeRecords.add(new ChangeRecord(DISCIPLINE, currentUser.getEmployeesPositions().getDiscipline().getValue(),
                    dto.getDiscipline(), employeeHistoryRes));
        } else {
            changeRecords.add(new ChangeRecord(DISCIPLINE, null,
                    dto.getDiscipline(), employeeHistoryRes));
        }
        /**
         * Map EmployeesProjects to list of Strings
         */
        List<String> projects = currentUser.getEmployeesProjects()
                .stream()
                .map(p -> p.getProject().getName())
                .collect(Collectors.toList());
        /**
         * Check if projects do not contains new project
         */
        if (!projects.contains(dto.getProject())) {
            String oldProjects = String.join(", ", projects);
            changeRecords.add(new ChangeRecord(PROJECT, oldProjects,
                    dto.getProject(), employeeHistoryRes));
        }
        /**
         * Check if projects contains new project
         */
        if (projects.contains(dto.getProject())) {

            EmployeeProject project = currentUser.getEmployeesProjects()
                    .stream()
                    .filter(e -> e.getProject().getName().equals(dto.getProject())).findFirst()
                    .orElseThrow(() -> new UsernameNotFoundException("Project not found"));


            String startDateNew = dto.getStartDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String endDateNew = dto.getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            String startDateOld = project.getStartDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String endDateOld = project.getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            addOldAndNewProjectDates(employeeHistoryRes, changeRecords, startDateNew, endDateNew, startDateOld, endDateOld);
        }
        /**
         * Check EmployeesPositions is not null
         * and Status is not equals to new Status
         */
        if (currentUser.getEmployeesPositions() != null &&
                !dto.getStatus().equals(currentUser.getEmployeesPositions().getStatus().getValue())) {
            changeRecords.add(new ChangeRecord(STATUS,
                    currentUser.getEmployeesPositions().getStatus().getValue(),dto.getStatus(),
                    employeeHistoryRes));
        } else {
            changeRecords.add(new ChangeRecord(STATUS,
                    null,dto.getStatus(), employeeHistoryRes));
        }
        /**
         * Check if password is empty
         */
        if (!dto.getPassword().isEmpty()) {
            changeRecords.add(new ChangeRecord(PASSWORD,
                    null, PASSWORD_HAS_BEEN_CHANGED, employeeHistoryRes));
        }
        /**
         * Save ChangeRecords to DB
         */
        List<ChangeRecord> changeRecordRes = this.changeRecordService.saveAll(changeRecords);
        employeeHistoryRes.setChangeRecord(changeRecordRes);
        log.info("Add audit record when editing a user");

        return this.managementRepository.save(employeeHistoryRes);
    }
    /**
     *  Adds old and new project dates to change record
     * @param employeeHistoryRes receives EmployeeHistory
     * @param changeRecords receives list of ChangeRecord
     * @param startDateNew receives start Date
     * @param endDateNew receives end Date
     * @param startDateOld receives old start Date
     * @param endDateOld receives old end Date
     */
    private void addOldAndNewProjectDates(EmployeeHistory employeeHistoryRes, List<ChangeRecord> changeRecords, String startDateNew, String endDateNew, String startDateOld, String endDateOld) {
        /**
         * Check new and old Dates and set them
         */
        if (!startDateOld.equals(startDateNew) || !endDateOld.equals(endDateNew)) {
            changeRecords.add(new ChangeRecord(PROJECT_START_DATE, startDateOld,
                    startDateNew, employeeHistoryRes));
            changeRecords.add(new ChangeRecord(PROJECT_END_DATE, endDateOld,
                    endDateNew, employeeHistoryRes));
        }
    }
}
