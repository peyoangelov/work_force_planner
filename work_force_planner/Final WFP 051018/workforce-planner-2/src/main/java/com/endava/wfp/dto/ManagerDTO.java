package com.endava.wfp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ManagerDTO {

    private Long id;

    private String firstName;

    private String lastName;
}
