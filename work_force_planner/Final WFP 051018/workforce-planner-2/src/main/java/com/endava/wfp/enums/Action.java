package com.endava.wfp.enums;

public enum Action {

    INSERTED("Inserted"),
    UPDATED("Updated"),
    DELETED("Deleted");
    private final String name;

    private Action(String value) {
        this.name = value;
    }

    public String value() {
        return this.name;
    }

    @Override
    public String toString() {
        return name;
    }
}