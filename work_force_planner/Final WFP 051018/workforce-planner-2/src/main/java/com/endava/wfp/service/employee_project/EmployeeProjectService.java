package com.endava.wfp.service.employee_project;

import com.endava.wfp.dto.ProjectEmployeeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeProjectService {

    /**
     * @param employeeId id of the employee
     *                   we want to
     *                   remove from project
     * @param projectId id of the selected project
     * Removes the employee
     * from the current
     * project
     */
    void delete(Long employeeId,Long projectId);
    /**
     *
     * @param projectId project id
     *                  of the selected project
     * @param pageable variable for
     *                 pagination
     * @return Page with projects
     *
     * Function which takes
     * a project and
     * finds all employees
     * assigned to
     * that project
     */
    Page<ProjectEmployeeDTO> findAllByProject(Long projectId, Pageable pageable);
}
