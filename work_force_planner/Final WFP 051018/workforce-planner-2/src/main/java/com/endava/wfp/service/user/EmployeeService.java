package com.endava.wfp.service.user;

import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.entity.Employee;

import java.util.List;

/**
 * Interface for EmployeeServiceImpl class for override methods
 */
public interface EmployeeService {
    Employee findById(Long id);
    Employee add(EmployeeAddDTO model);
    void delete(Long id);
    void edit(EmployeeEditDTO model) throws IllegalAccessException;
    List<Employee> getAllManagers();
    boolean isManager(Long id);
}