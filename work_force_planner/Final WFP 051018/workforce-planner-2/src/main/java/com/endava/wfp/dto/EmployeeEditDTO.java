package com.endava.wfp.dto;

import com.endava.wfp.dto.interfaces.IStartEndDateForm;
import com.endava.wfp.dto.interfaces.IStatusProjectForm;
import com.endava.wfp.utility.Constants;
import com.endava.wfp.utility.validations.StartEndDateConstraint;
import com.endava.wfp.utility.validations.StatusConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
/**
 * DTO used for editing
 * the data for a given
 * employee
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@StartEndDateConstraint //StartDate and EndDate validation
@StatusConstraint //Status and Project validation
public class EmployeeEditDTO implements IStartEndDateForm, IStatusProjectForm {

    private Long id;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String firstName;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String lastName;

    @NotNull(message = Constants.BLANK_FIELD)
    @Pattern(regexp="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}")
    private String email;

    @NotBlank(message = Constants.BLANK_FIELD)
    @NotNull(message = Constants.BLANK_FIELD)
    private String password;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String grade;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String status;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String discipline;

    @NotNull(message = Constants.BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = Constants.BLANK_FIELD)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @NotNull(message = Constants.BLANK_FIELD)
    @NotBlank(message = Constants.BLANK_FIELD)
    private String project="Current Bench";

    private String skill;
}
