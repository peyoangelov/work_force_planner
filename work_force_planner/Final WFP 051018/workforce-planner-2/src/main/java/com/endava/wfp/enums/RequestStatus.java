package com.endava.wfp.enums;

public enum RequestStatus {

    APPROVED("Approved"),
    PENDING("Pending"),
    REJECTED("Rejected");

    private final String value;

    RequestStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
