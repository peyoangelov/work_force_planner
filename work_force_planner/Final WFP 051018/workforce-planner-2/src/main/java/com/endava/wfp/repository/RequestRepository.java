package com.endava.wfp.repository;

import com.endava.wfp.entity.Request;
import com.endava.wfp.enums.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {
    List<Request> findByStatus(RequestStatus status);
    Boolean existsByEmail(String email);
}