package com.endava.wfp.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "change_record")
public class ChangeRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String propertyName;

    @Column
    private String oldValue;

    @Column
    private String newValue;

    @ManyToOne
    @JoinColumn(name = "employee_history_id")
    private EmployeeHistory employeeHistory;

    public ChangeRecord(String propertyName, String oldValue, String newValue, EmployeeHistory employeeHistory) {
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.employeeHistory = employeeHistory;
    }

    public ChangeRecord() {
    }
}
