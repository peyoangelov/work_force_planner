package com.endava.wfp.service.management;

import com.endava.wfp.entity.ChangeRecord;

import java.util.List;

/**
 * Interface for ChangeRecordServiceImpl class for override methods
 */
public interface ChangeRecordService {
    List<ChangeRecord> saveAll(List<ChangeRecord> changeRecords);
}
