package com.endava.wfp.service.user;

import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.entity.*;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Grade;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.*;
import com.endava.wfp.service.management.ManagementService;
import com.endava.wfp.service.project.ProjectService;
import com.endava.wfp.utility.exception.StartAndEndDateException;
import com.endava.wfp.utility.exception.UsedEmailException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EmployeeServiceImpl implements EmployeeService, UserDetailsService {

    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder encoder;
    private final SkillRepository skillRepository;
    private final PositionRepository positionRepository;
    private final ProjectRepository projectRepository;
    private final EmployeeProjectRepository employeeProjectRepository;
    private final ManagementService managementService;
    private final ProjectService projectService;

    /**
     * Accept new parameters and edit Employee
     * @param editDto receives EmployeeEditDTO
     * @throws IllegalAccessException
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional
    public void edit(EmployeeEditDTO editDto) throws IllegalAccessException, UsernameNotFoundException {
        log.info("Find employee by email.");
        /**
         * Find employee by email
         */
        Employee currentUser = this.employeeRepository.findFirstByEmail(currentUserDetails().getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
        /**
         * Check if user is Manager
         */
        if (isManager(currentUser.getId())) {
            Employee employee = this.employeeRepository.findFirstByEmail(editDto.getEmail())
                    .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
            log.info("Record employee in Audit.");
            /**
             * audit Employee data for history
             */
            this.managementService.editEmployeeAudit(employee, editDto);

            setNewPassword(editDto, employee);
            createEmployeePositions(editDto, employee);

            employee.getEmployeesPositions().setGrade(Grade.fromString(editDto.getGrade()));
            employee.getEmployeesPositions().setDiscipline(Discipline.fromString(editDto.getDiscipline()));
            employee.getEmployeesPositions().setStatus(Status.fromValue(editDto.getStatus()));

            log.info("Find employee project.");
            /**
             * Find project by name
             */
            Project project = this.projectRepository.findByName(editDto.getProject());

            createEmployeeProjects(employee, project);
            /**
             * Set Employee project
             */
            employee.getEmployeesProjects()
                    .stream()
                    .filter(e -> e.getEmployee().getId().equals(editDto.getId()))
                    .findFirst()
                    .get()
                    .setProject(project);
            /**
             * Set employee Date
             */
            employee.getEmployeesProjects().stream()
                    .filter(ep -> ep.getProject().getName().equals(editDto.getProject()))
                    .forEach(e -> {
                        e.setStartDate(editDto.getStartDate());
                        e.setEndDate(editDto.getEndDate());
                    });
            log.info("Save employee with new data in DB.");
            /**
             * Save employee in DB
             */
            this.employeeRepository.save(employee);
        } else {
            throw new IllegalAccessException("You are not a manager");
        }
    }
    /**
     * Sets new password
     * @param editDto receives EmployeeEditDTO
     * @param employee receives Employee
     */
    private void setNewPassword(EmployeeEditDTO editDto, Employee employee) {
        /**
         * Check if password is empty
         */
        if (!"".equals(editDto.getPassword())) {
            employee.setPassword(this.encoder.encode(editDto.getPassword()));
        }
    }
    /**
     * Creates and saves new employee projects
     * @param employee receives Employee
     * @param project receives Project
     */
    private void createEmployeeProjects(Employee employee, Project project) {
        /**
         * Check if EmployeesProjects is empty
         */
        if (employee.getEmployeesProjects().isEmpty()) {
            EmployeeProject employeeProject = new EmployeeProject();
            employeeProject.setEmployee(employee);
            employeeProject.setProject(project);
            /**
             * Save EmployeeProject in DB
             */
            EmployeeProject savedProject = this.employeeProjectRepository.save(employeeProject);
            employee.getEmployeesProjects().add(savedProject);
        }
    }
    /**
     * Create new EmployeePositions
     * @param editDto EmployeeEditDTO
     * @param employee Employee
     */
    private void createEmployeePositions(EmployeeEditDTO editDto, Employee employee) {
        /**
         * Check if getEmployeesPositions is null
         */
        if (employee.getEmployeesPositions() == null) {
            EmployeePosition employeePosition = new EmployeePosition();
            employeePosition.setEmployee(employee);
            /**
             * Find Skills by name
             */
            Skill skill = this.skillRepository.findByName(editDto.getSkill());
            List<Skill> skills = new ArrayList<>();
            skills.add(skill);
            employeePosition.setSkills(skills);
            /**
             * Save EmployeePosition in DB
             */
            EmployeePosition savedPosition = this.positionRepository.save(employeePosition);
            employee.setEmployeesPositions(savedPosition);
        }
    }
    /**
     * Check if Employee is Manager my id
     * @param id receives id
     * @return true or false
     */
    @Override
    public boolean isManager(Long id) {
        log.info("Return if employee is manager");
        /**
         * Find Employee by id
         */
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
        return employee.getManager() == null;
    }
    /**
     * Accept id, find and return Employee
     * @param id receives id
     * @return Employee
     */
    @Override
    public Employee findById(Long id) {
        log.info("Return employee");
        return this.employeeRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
    }
    /**
     * Find all Employee who are Managers
     * @return list of Employee
     */
    @Override
    public List<Employee> getAllManagers() {
        log.info("Return all Managers");
        /**
         * Find Employees who manager id is null
         */
       return this.employeeRepository.findByManagerId(null)
               .stream()
               .sorted(Comparator.comparing(Employee::getFirstName).thenComparing(Employee::getLastName))
               .collect(Collectors.toList());
    }
    /**
     * Configure security and Authentication
     * @return UserDetails
     * @throws IllegalStateException
     */
    public static UserDetails currentUserDetails() throws IllegalStateException {
        log.info("Configure security and Authentication");
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        /**
         * Check authentication is not null
         */
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            return principal instanceof UserDetails ? (UserDetails) principal : null;
        }
        throw new IllegalStateException("No logged in user found");
    }
    /**
     * Accept new data and adding new Employee to DB
     * @param dto receives EmployeeAddDTO
     * @return Employee
     */
    @Override
    @Transactional
    public Employee add(EmployeeAddDTO dto) {
        log.info("Add new Employee");
        /**
         * Find Employee by id
         */
        Employee manager = this.employeeRepository.findById(dto.getManagerId())
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Employee manager not found for id %s",
                        dto.getManagerId())));
        checkForExistingEmail(dto.getEmail());

        if (!this.projectService.validateDates(dto.getStartDate(), dto.getEndDate(), dto.getProject())) {
            throw new StartAndEndDateException("Start and End date are not in the scope of Project date");
        }

        Employee employee = createEmployee(dto);
        employee.setManager(manager);
        /**
         * Save Employee in DB
         */
        Employee savedEmployee = this.employeeRepository.save(employee);
        EmployeePosition employeePosition = createEmployeePosition(savedEmployee, dto);
        EmployeeProject employeeProject = createEmployeeProjects(savedEmployee, dto);
        savedEmployee.setEmployeesPositions(employeePosition);

        List<EmployeeProject> list = new ArrayList<>();
        list.add(employeeProject);
        savedEmployee.setEmployeesProjects(list);
        /**
         * Save Employee in DB
         */
        this.managementService.saveHistoryAudit(savedEmployee);
        return this.employeeRepository.save(savedEmployee);
    }
    /**
     * Accept employee and dto with new data
     * and create EmployeeProject and save it to DB
     * @param employee receives Employee
     * @param dto receives EmployeeAddDTO
     * @return EmployeeProject
     */
    private EmployeeProject createEmployeeProjects(Employee employee, EmployeeAddDTO dto) {
        log.info("Create EmployeeProject and save it to DB.");
        EmployeeProject employeeProject = new EmployeeProject();
        employeeProject.setEmployee(employee);

        LocalDate startDate = dto.getStartDate();
        LocalDate endDate = dto.getEndDate();

        employeeProject.setStartDate(startDate);
        employeeProject.setEndDate(endDate);
        /**
         * Find project by name
         */
        Project project = this.projectRepository.findByName(dto.getProject());
        employeeProject.setProject(project);
        /**
         * Save EmployeeProject in DB
         */
        return this.employeeProjectRepository.save(employeeProject);
    }
    /**
     * Accept employee and dto with new data
     * and create EmployeePosition and save it to DB
     * @param employee receive Employee
     * @param dto receive EmployeeAddDTO
     * @return EmployeePosition
     */
    private EmployeePosition createEmployeePosition(Employee employee, EmployeeAddDTO dto) {
        log.info("Create EmployeePosition and save it to DB.");
        EmployeePosition position = new EmployeePosition();
        position.setGrade(Grade.fromString(dto.getGrade()));
        position.setDiscipline(Discipline.fromString(dto.getDiscipline()));
        position.setStatus(Status.fromValue(dto.getStatus()));
        position.setEmployee(employee);
        /**
         * Find Skill by name
         */
        Skill skill = this.skillRepository.findByName(dto.getSkill());
        List<Skill> list = new ArrayList<>();
        list.add(skill);
        position.setSkills(list);
        /**
         * Save EmployeePosition in DB
         */
        return this.positionRepository.save(position);
    }
    /**
     * Accept new data for employee, create and return it
     * @param dto receives EmployeeAddDTO
     * @return Employee
     */
    private Employee createEmployee(EmployeeAddDTO dto) {
        log.info("Create and return Employee");
        Employee employee = new Employee();
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        /**
         * Encode password
         */
        employee.setPassword(encoder.encode(dto.getPassword()));
        return employee;
    }
    /**
     * Accept id and delete Employee from DB
     * @param id receives id
     */
    @Override
    public void delete(Long id) {
        log.info("Delete Employee");
        /**
         * Find Employee by id
         */
        Employee employee = this.employeeRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
        if(!isManager(id)) {
            /**
             * Audit Employee history
             */
            this.managementService.deleteEmployeeAudit(employee);
        }
        /**
         * Delete Employee
         */
        this.employeeRepository.deleteById(id);
    }
    /**
     * Accept email and create Roles form user
     * @param email receives user Email
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.info("Create Roles form user");
        /**
         * Find Employee by email
         */
        Employee employee = this.employeeRepository.findFirstByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));

        String role = isManager(employee.getId()) ? "ROLE_ADMIN" : "ROLE_USER";
        Set<GrantedAuthority> roles = Collections.singleton(new SimpleGrantedAuthority(role));
        return new User(employee.getEmail(), employee.getPassword(), roles);
    }
    /**
     * Check email and if not exist throw Exception
     * @param email receives user Mail
     */
    void checkForExistingEmail(String email) {
        /**
         * Check if mail exist
         */
        if(employeeRepository.existsByEmailIgnoreCase(email)) {
            log.info("Throw UsedEmailException");
            throw new UsedEmailException("There is an employee with the same email");
        }
    }
}