package com.endava.wfp.controllers;

import com.endava.wfp.dto.EmployeePageDTO;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.utility.Constants;
import com.endava.wfp.view.ExcelExportView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@Slf4j
public class ExcelExportController {

    EmployeePagination employeePagination;

    public ExcelExportController(EmployeePagination employeePagination) {
        this.employeePagination = employeePagination;
    }
    /**
     *
     * @param page the current page
     *             in list of employees
     *
     * @param sortingProperty the current sorting property
     *
     * @return sends the excel file
     */
    @GetMapping("/list/exportToExcel/{page}/{sortingProperty}")
    public ModelAndView exportToExcel(@PathVariable Integer page, @PathVariable String sortingProperty) {

        log.info("Export list of employees in excel file.");
        Pageable pageable = PageRequest.of(page, Constants.EMPLOYEE_PER_PAGE, Sort.by(sortingProperty.split(",")).ascending());
        Page<EmployeePageDTO> employees = employeePagination.findAll(pageable);
        List<EmployeePageDTO> listOfEmployees = employees.getContent();

        return new ModelAndView(new ExcelExportView(), "listOfEmployees", listOfEmployees);
    }
}

