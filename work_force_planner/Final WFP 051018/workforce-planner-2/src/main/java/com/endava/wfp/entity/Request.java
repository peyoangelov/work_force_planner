package com.endava.wfp.entity;

import com.endava.wfp.enums.RequestStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

import static com.endava.wfp.utility.Constants.*;

/**
 * Entity corresponding to
 * table requests with
 * its relationships in the
 * database
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "requests")

public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Size(min = 1, max = 100, message = FIRST_NAME_SIZE_MESSAGE)
    @Column(name = "first_name")
    @NotNull(message = BLANK_FIELD)
    private String firstName;

    @Size(min = 1, max = 100, message = LAST_NAME_SIZE_MESSAGE)
    @Column(name = "last_name")
    @NotNull(message = BLANK_FIELD)
    private String lastName;

    @Size(min = 1, max = 100, message = E_MAIL_SIZE_MESSAGE)
    @Column(name = "email")
    @NotNull(message = BLANK_FIELD)
    @Valid
    private String email;

    @Size(max = 300)
    @Column(name = "reason_for_request")
    @NotNull(message = BLANK_FIELD)
    private String reasonForRequest;

    @Enumerated(EnumType.STRING)
    @NotNull(message = BLANK_FIELD)
    private RequestStatus status;

    @CreatedDate
    @NotNull(message = BLANK_FIELD)
    private LocalDateTime requestDate;

    @Basic(optional = true)
    @CreatedDate
    @Column(name = "revised_request_date_time")
    private LocalDateTime revisedDateTime;

    @Basic(optional = true)
    @Size(min = 1, max = 100)
    @Column(name = "manager_email")
    private String revisedManagerEmail;

    @Basic(optional = true)
    @Size(min = 1, max = 100)
    @Column(name = "manager_fullname")
    private String revisedManagerFullName;
}
