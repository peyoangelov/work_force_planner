package com.endava.wfp.model;

import com.endava.wfp.utility.Constants;
import lombok.Data;

@Data
public class PageCreator {

    private int startPage;

    private int endPage;

    public PageCreator(int totalPages, int currentPage) {

        int halfPagesToShow = Constants.buttonsToShow / 2;

        if (totalPages <= Constants.buttonsToShow) {
            startPage = 1;
            endPage = totalPages;

        } else if (currentPage - halfPagesToShow <= 0) {
            startPage = 1;
            endPage = Constants.buttonsToShow;

        } else if (currentPage + halfPagesToShow == totalPages) {
            startPage = currentPage - halfPagesToShow;
            endPage = totalPages;

        } else if (currentPage + halfPagesToShow > totalPages) {
            startPage = totalPages - Constants.buttonsToShow + 1;
            endPage = totalPages;

        } else {
            startPage = currentPage - halfPagesToShow;
            endPage = currentPage + halfPagesToShow;
        }

    }

}
