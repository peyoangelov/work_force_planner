package com.endava.wfp.dto;

import com.endava.wfp.enums.Discipline;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 */
@AllArgsConstructor
@Data
public class DistributionDTO {

    private Discipline discipline;
    private Long count;
}
