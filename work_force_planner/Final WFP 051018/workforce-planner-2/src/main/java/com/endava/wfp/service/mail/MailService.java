package com.endava.wfp.service.mail;

import com.endava.wfp.dto.EmployeeAddDTO;
import com.endava.wfp.dto.EmployeeEditDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.enums.MailType;

import javax.mail.MessagingException;

/**
 * Interface for MailServiceImpl class for override methods
 */
public interface MailService {
    boolean sendMailMessage(MailType messageType, String recipient, EmployeeEditDTO dto) throws MessagingException;
    boolean sendMailMessage(MailType messageType, String recipient, EmployeeAddDTO dto) throws MessagingException;
    boolean sendMailMessage(MailType messageType, String recipient, Employee employee) throws MessagingException;
}
