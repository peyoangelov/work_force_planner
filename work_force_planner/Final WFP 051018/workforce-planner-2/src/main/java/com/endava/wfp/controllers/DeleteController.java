package com.endava.wfp.controllers;

import com.endava.wfp.dto.PageRedirectDTO;
import com.endava.wfp.service.user.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class DeleteController {

    private final EmployeeService employeeService;
    /**
     * Deleting employee
     *
     * @param dto pageRedirectDto where
     *            id, sortingProperty and
     *            page is stored
     *
     * @return redirects to front end
     */
    @PostMapping("/list/delete")
    public ModelAndView delete(PageRedirectDTO dto) {

        if (this.employeeService.isManager(dto.getId())) {
            return new ModelAndView("redirect:/list/" + dto.getPage() + "/" + dto.getSortingProperty() + "?error=true");
        } else {
            log.info("Deleting employee {}", dto);
            this.employeeService.delete(dto.getId());
            return new ModelAndView("redirect:/list/" + dto.getPage() + "/" + dto.getSortingProperty() + "?deleted=true");
        }
    }
}
