package com.endava.wfp.enums;

public enum Grade {
    JUNIOR("Junior"),
    MID("Mid"),
    SENIOR("Senior");

    private String value;

    Grade(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Grade fromString(String name) {
        for(Grade g : Grade.values()) {
            if(g.value.equalsIgnoreCase(name)) {
                return g;
            }
        }
        throw new IllegalArgumentException("No constant with text " + name + " found");
    }
}