package com.endava.wfp.service.project;

import com.endava.wfp.dto.ProjectAddDTO;
import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.EmployeeProject;
import com.endava.wfp.entity.Project;
import com.endava.wfp.enums.MailType;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.EmployeeProjectRepository;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.repository.ProjectRepository;
import com.endava.wfp.service.mail.MailService;
import com.endava.wfp.utility.exception.ExistingProjectException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.validation.ValidationException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final ModelMapper modelMapper;
    private final EmployeeProjectRepository employeeProjectRepository;
    private final EmployeeRepository employeeRepository;
    private final MailService mailService;

    /**
     * Return all projects
     * @return list of ProjectNameDTO
     */
    @Override
    public List<ProjectNameDTO> findAll() {
        log.info("Return all projects.");
        /**
         * Map Project to ProjectNameDTO
         */
        List<Project> projects = this.projectRepository.findAll();
        return projects.stream()
                .map(p -> this.modelMapper.map(p, ProjectNameDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * Adds employees in employees_projects
     * table if valid dates were being
     * filled in by the user
     *
     * @param ids         chosen employees' ids
     *                    with checkboxes
     * @param startDates  chosen start dates
     *                    for every chosen employee
     *                    with checkboxes
     * @param endDates    chosen end dates
     *                    for every chosen employee
     *                    with checkboxes
     * @param projectName project's name
     */
    @Override
    public void addEmployeesToProject(ArrayList<Long> ids, ArrayList<String> startDates, ArrayList<String> endDates, String projectName) {

        Project project = projectRepository.findByName(projectName);

        for (int i = 0; i < ids.size(); i++) {
            if (validateDates(LocalDate.parse(startDates.get(i)), LocalDate.parse(endDates.get(i)), projectName)) {
                EmployeeProject add = new EmployeeProject();
                add.setStartDate(LocalDate.parse(startDates.get(i)));
                add.setEndDate(LocalDate.parse(endDates.get(i)));
                add.setProject(project);
                Employee employee = employeeRepository.findById(ids.get(i))
                        .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
                add.setEmployee(employee);

                changeStatus(employee, LocalDate.parse(startDates.get(i)), LocalDate.parse(endDates.get(i)));
                employeeProjectRepository.saveAndFlush(add);
                try {
                    this.mailService.sendMailMessage(MailType.EMPLOYEE_ASSIGNED, employee.getEmail(), employee);
                } catch (MessagingException e) {
                    log.error("Sending e-mail failed!",e);
                }
            } else {
                throw new ValidationException("Dates are no validated!");
            }
        }
    }

    /**
     * Changes status of the chosen employee
     * according to his current status and
     * chosen start date and end date
     *
     * @param employee  chosen employee by the user
     * @param startDate employee's start date
     *                  on a project
     * @param endDate   employee's end date
     *                  on a project
     */
    @Override
    public void changeStatus(Employee employee, LocalDate startDate, LocalDate endDate) {

        switch (employee.getEmployeesPositions().getStatus()) {
            case CURRENT_BENCH:
                if (startDate.isBefore(LocalDate.now().plusMonths(1))) {
                    employee.getEmployeesPositions().setStatus(Status.BOOKED);
                } else {
                    employee.getEmployeesPositions().setStatus(Status.BOOKED_IN_ONE_MONTH);
                }
                break;
            case BOOKED_IN_ONE_MONTH:
                if (endDate.isBefore(LocalDate.now().plusMonths(1))) {
                    employee.getEmployeesPositions().setStatus(Status.BOOKED);
                } else {
                    throw new ValidationException("Validation of employee's end date failed!");
                }
                break;
            case BENCH_IN_ONE_MONTH:
                if (startDate.isAfter(LocalDate.now().plusMonths(1))) {
                    employee.getEmployeesPositions().setStatus(Status.BOOKED);
                } else {
                    throw new ValidationException("Validation of employee's start date failed!");
                }
                break;
            case BENCH_IN_TWO_MONTHS:
                if (startDate.isAfter(LocalDate.now().plusMonths(2))) {
                    employee.getEmployeesPositions().setStatus(Status.BOOKED);
                } else {
                    throw new ValidationException("Validation of employee's start date failed!");
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param startDate   employee's start date
     *                    on a project
     * @param endDate     employee's end date
     *                    on a project
     * @param projectName project's name
     * @return true or false depending on
     * that if the validation of
     * employees' dates are responding
     * to the project's start and end dates
     */
    @Override
    public Boolean validateDates(LocalDate startDate, LocalDate endDate, String projectName) {
        Project project = projectRepository.findByName(projectName);
        LocalDate projectStartDate = project.getStartDate();
        LocalDate projectEndDate = project.getEndDate();

        return (startDate != null
                && endDate != null
                && startDate.isAfter((LocalDate.now()).minusDays(1))
                && startDate.isAfter((projectStartDate).minusDays(1))
                && endDate.isBefore((projectEndDate).plusDays(1))
                && (startDate.isBefore(endDate)));
    }

    /**
     * Returns a new project entry in the database
     * @param model receives data for new Employee
     * @return Project
     */
    @Override
    public Project add(ProjectAddDTO model) {

        checkForExistingProject(model.getProjectName());
        Project project = createNewProject(model);
        return this.projectRepository.save(project);
    }

    /**
     * Helper function
     * to create new project
     * @param model the dto passed
     *              from the html
     * @return returns the filled
     * data for the project
     */
    private Project createNewProject(ProjectAddDTO model){
        Project project = new Project();
        project.setName(model.getProjectName());
        project.setDescription(model.getDescription());
        project.setStartDate(model.getStartDate());
        project.setEndDate(model.getEndDate());
        return project;
    }

    /**
     * Function checks if the same project already exists
     * @param projectName receives the name of the Project
     */
    private void checkForExistingProject(String projectName) {
        if (projectRepository.findByName(projectName) != null) {
            throw new ExistingProjectException("Project with the same name already exists!");
        }
    }

    /**
     * Find all projects in DB
     * @param pageable receives pageable
     * @return Page with all Projects name
     */
    @Override
    public Page<ProjectNameDTO> findAllProjectByPage(Pageable pageable) {
        Page<Project> allProjects = this.projectRepository.findAll(pageable);
        return allProjects.map(record -> modelMapper.map(record, ProjectNameDTO.class));
    }
}
