package com.endava.wfp.repository;

import com.endava.wfp.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    Optional<Employee> findFirstByEmail(String email);
    Boolean existsByEmail(String email);

    Boolean existsByEmailIgnoreCase(String email);

    List<Employee> findByManagerId(@Nullable Long id);
}


