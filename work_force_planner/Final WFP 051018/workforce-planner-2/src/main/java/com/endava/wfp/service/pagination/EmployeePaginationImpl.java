package com.endava.wfp.service.pagination;

import com.endava.wfp.dto.EmployeePageDTO;
import com.endava.wfp.entity.Employee;
import com.endava.wfp.entity.Skill;
import com.endava.wfp.enums.Status;
import com.endava.wfp.repository.EmployeeRepository;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class EmployeePaginationImpl implements EmployeePagination {

    private final EmployeeRepository employeeRepository;
    private final ModelMapper modelMapper;
    /**
     * Accept pageable and Find all employees and return page with employee dtos
     * @param pageable receives pageable interface
     * @return Page of EmployeePageDTO
     */
    @Override
    public Page<EmployeePageDTO> findAll(Pageable pageable) {
        /**
         * Find all employees and set them into the Page
         */
        Page<Employee> employeesEntities = this.employeeRepository.findAll(pageable);
        log.info("Accept pageable and Find all employees and return page with employee dtos.");
        /**
         * Map Employee to EmployeePageDTO
         */
        return employeesEntities.map(employee -> {
            try {
                EmployeePageDTO employeePageDTO = modelMapper.map(employee, EmployeePageDTO.class);
                setSkills(employee, employeePageDTO);

                return employeePageDTO;
            } catch (NullPointerException ex) {
                return null;
            }
        });
    }
    /**
     * Set skills if employee position is not null
     * @param employee receives Employee
     * @param employeePageDTO receives EmployeePageDTO with new parameters
     */
    private void setSkills(Employee employee, EmployeePageDTO employeePageDTO) {
        /**
         * Check EmployeesPositions if is not null
         * and set Skills
         */
        if(employee.getEmployeesPositions() != null) {
            employeePageDTO
                    .setSkills(employee
                            .getEmployeesPositions()
                            .getSkills()
                            .stream()
                            .map(Skill::getName)
                            .collect(Collectors.joining(", ")));
        }
    }
    /**
     * Accept Page, Integer with specific page number and redirect to specific page
     * @param pageList receives Page with different Generic type
     * @param selectedPage receives Integer with page number
     * @return ModelAndView with set params
     */
    @Override
    public ModelAndView redirectSearchPage(Page<?> pageList, Integer selectedPage, String redirectionPage) {
        log.info("Accept Page, Integer with specific page number and redirect to specific page.");
        /**
         * Check selected page is less than Zero
         */
        if (selectedPage < Constants.ZERO_PAGE) {
            selectedPage = Constants.FIRST_PAGE;
        } else if (selectedPage > pageList.getTotalPages()) {
            selectedPage = pageList.getTotalPages();
        }

        return new ModelAndView("redirect:/" +
                redirectionPage +
                "/" +
                selectedPageInBounds(pageList, selectedPage));
    }
    /**
     * Accept Page, Integer with specific page number, Sorting param and redirect to specific page
     * @param pageList receives Page with different Generic type
     * @param selectedPage receives Integer with page number
     * @param sortingProperty receives Sorting property
     * @return ModelAndView with params to redirect page
     */
    @Override
    public ModelAndView redirectSearchPageBySortingProperty(Page<?> pageList,
                                                            Integer selectedPage,
                                                            String sortingProperty,
                                                            String redirectionPage) {

        log.info("Accept Page, Integer with specific page number, Sorting param and redirect to specific page.");
        /**
         * Check if page number is less than Zero
         */
        if (selectedPage < Constants.ZERO_PAGE) {
            selectedPage = Constants.FIRST_PAGE;
        } else if (selectedPage > pageList.getTotalPages()) {
            selectedPage = pageList.getTotalPages();
        }
        return new ModelAndView("redirect:/" +
                redirectionPage +
                "/" +
                selectedPageInBounds(pageList, selectedPage) + "/" + sortingProperty);
    }
    /**
     *
     * @param pageList list of pages
     * @param selectedPage pagination
     *                     input for "Go" button
     * @return
     */
    @Override
    public Integer selectedPageInBounds(Page<?> pageList, Integer selectedPage) {
        if (selectedPage < Constants.ZERO_PAGE) {
            return Constants.FIRST_PAGE;
        } else if (selectedPage > pageList.getTotalPages()) {
            return pageList.getTotalPages();
        }
        return selectedPage;
    }
    /**
     *
     * @param pageList  list of pages
     * @param selectedPage  selectedPage pagination
     *          input for "Go" button
     * @param redirectionPage String
     *                        to which functionality to redirect
     * @param project project
     * @return
     */
    @Override
    public ModelAndView redirectSearchPageByProject(Page<?> pageList ,Integer selectedPage, String redirectionPage, String project) {

        return new ModelAndView("redirect:/" +
                redirectionPage +
                "/" +
                project +
                "/" +
                selectedPageInBounds(pageList, selectedPage));
    }
    /**
     * @param pageList          all pages
     * @param selectedPage      page that was filled
     *                          in the text box
     * @param filteringProperty filters the employees
     *                          by chosen status
     * @return redirects to a page
     * according to what was filled in
     * the text box and does not change
     * the filtration
     */
    @Override
    public ModelAndView redirectSearchPageByFilteringProperty(Page<?> pageList, Integer projectsPage, Integer selectedPage, Status filteringProperty, String projectName) {
        if (selectedPage < Constants.ZERO_PAGE) {
            selectedPage = Constants.FIRST_PAGE;
        } else if (selectedPage > pageList.getTotalPages()) {
            selectedPage = pageList.getTotalPages();
        }

        return new ModelAndView("redirect:/assign/" + projectsPage + "/" + projectName + "/" + selectedPage + "/" + filteringProperty);
    }
}
