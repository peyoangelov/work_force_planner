package com.endava.wfp.utility.validations;

import com.endava.wfp.dto.interfaces.IStatusProjectForm;
import com.endava.wfp.enums.Status;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StatusValidator implements ConstraintValidator<StatusConstraint, Object> {

    private static final String CURRENT_BENCH = "Current Bench";
    /**
     /**
     * Check if start Data and End Date are valid
     * @param object The object received
     *               from the place where
     *               the annotation was added
     * @param constraintValidatorContext this is the
     *                                   anntoation 
     *                                   interface
     * @return  true or false
     *          returns if the
     *          following class fields
     *          are valid
     */
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        IStatusProjectForm statusProjectForm = (IStatusProjectForm) object;
        String status = statusProjectForm.getStatus();
        String project = statusProjectForm.getProject();
        /*
            If either one is empty runtime continues until it reaches @NotNUll
        */
        if(status == null || project == null){
            return true;
        }
        return checkProjectAndStatus(project,status);
    }
    /**
     * Check project and status is current bench
     * @param project the project for
     *                which we are checking
     *                the current status
     * @param status the status on the
     *               current project
     * @return      true if the
     *              Project is not
     *              current bench
     *              when the status
     *              is not current bench
     *              or false when the project
     *              is current bench but the
     *              status is not Current Bench
     */
    private boolean checkProjectAndStatus(String project, String status) {
        return !(!Status.CURRENT_BENCH.getValue().equals(status) && project.equals(CURRENT_BENCH));
    }
}
