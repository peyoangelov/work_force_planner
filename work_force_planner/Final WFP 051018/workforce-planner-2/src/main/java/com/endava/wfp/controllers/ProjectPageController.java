package com.endava.wfp.controllers;

import com.endava.wfp.dto.ProjectNameDTO;
import com.endava.wfp.model.PageCreator;
import com.endava.wfp.service.pagination.EmployeePagination;
import com.endava.wfp.service.project.ProjectService;
import com.endava.wfp.utility.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectPageController {

    private final ProjectService projectService;
    private final EmployeePagination employeePagination;

    /**
     * Projects page controller
     * showing
     * @param page
     * @param selectedPage
     * @return
     */
    @GetMapping("/projects/{page}")
    public ModelAndView management(@PathVariable("page") Integer page, @RequestParam(value = "selectedPage", required = false) Integer selectedPage) {
        ModelAndView modelAndView = new ModelAndView("project/project");

        final String REDIRECT_PAGE_PROJECTS = "projects";

        if (page < Constants.FIRST_PAGE) {
            page = Constants.FIRST_PAGE;
        }

        int pageIndex = page - 1;

        Page<ProjectNameDTO> allProjectsOnPage = projectService.findAllProjectByPage(PageRequest.of(pageIndex, Constants.EMPLOYEE_PER_PAGE, Sort.by("id").descending()));
        /**
         * The count of page is starting with zero, that's why we add +1 in order to make the page count correct
         */
        PageCreator pageCreator = new PageCreator(allProjectsOnPage.getTotalPages() , allProjectsOnPage.getNumber() + 1);

        modelAndView.addObject("allProjectsOnPage", allProjectsOnPage);
        modelAndView.addObject("pageCreator", pageCreator);
        modelAndView.addObject("pageNumber", allProjectsOnPage.getNumber());
        modelAndView.addObject("maxPages", allProjectsOnPage.getTotalPages());

        if (selectedPage != null) {
            modelAndView = employeePagination.redirectSearchPage(allProjectsOnPage, selectedPage, REDIRECT_PAGE_PROJECTS);
        }
        return modelAndView;
    }
}