package com.endava.wfp.dto;


import lombok.Data;

/**
 * DTO used to store redirection
 * information for url
 */
@Data
public class PageRedirectDTO {

    private Long id;

    private Long page;

    private String sortingProperty;
}
