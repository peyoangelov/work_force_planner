package com.endava.wfp.entity;

import com.endava.wfp.enums.Action;
import com.endava.wfp.utility.EmployeeHistoryEventListener;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "management_details")
@EntityListeners(value = EmployeeHistoryEventListener.class)
@NoArgsConstructor
public class EmployeeHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_id")
    private Long employeeId;

    @CreatedDate
    private LocalDateTime modifiedDate;

    @OneToMany(mappedBy = "employeeHistory", cascade = CascadeType.ALL)
    private List<ChangeRecord> changeRecord;

    @Enumerated(STRING)
    private Action action;

    @Column(name = "full_name")
    private String fullName;
}
