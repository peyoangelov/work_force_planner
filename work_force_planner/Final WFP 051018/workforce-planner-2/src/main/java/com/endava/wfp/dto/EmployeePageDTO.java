package com.endava.wfp.dto;

import com.endava.wfp.entity.Employee;
import com.endava.wfp.enums.Discipline;
import com.endava.wfp.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * DTO used for
 * employee list
 * pagination
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"employeesPositionsDiscipline", "employeesPositionsStatus"})
public class EmployeePageDTO {
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private Discipline employeesPositionsDiscipline;

    private Status employeesPositionsStatus;

    private String skills;

    private Employee manager;

    public String[] toStringArray() {
        String[] employeePageDTOArray = new String[5];

        employeePageDTOArray[0] = this.getFirstName() + " " + this.getLastName();
        employeePageDTOArray[1] = this.getEmail();
        employeePageDTOArray[2] = this.getEmployeesPositionsDiscipline() == null ? "" : this
                .getEmployeesPositionsDiscipline()
                .getValue();
        employeePageDTOArray[3] = this.getEmployeesPositionsStatus() == null ? "" : this
                .getEmployeesPositionsStatus()
                .getValue();
        employeePageDTOArray[4] = this.getSkills();


        return employeePageDTOArray;
    }
}
